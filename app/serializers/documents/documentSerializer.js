import DocumentFeaturesSerializer from 'serializers/documents/documentFeaturesSerializer';

const documentSerializer = document => {
  if (!document) {
    return {};
  }

  const data = {
    type: 'documents',
    id: document.id,
    name: document.name,
    link: document.link,
    usageName: document.usageName,
    nbPage: document.nbPage,
    public: document.public,
    description: document.description,
    status: document.status,
    category: document.category,
    idMasterTranslation: document.idMasterTranslation,
    translationHistory: document.translationHistory,
    idTranslation: document.idBranches,
    idOwner: document.idOwner,
    creationDate: document.creationDate,
    editDate: document.editDate,
    timestamp: document.timestamp,
    access: {
      role: document.access.role,
      moderators: document.access.moderator,
      collaborator: document.access.collaborator
    },
    features: DocumentFeaturesSerializer(document.features)
  };
  return data;
};

export default documentSerializer;
