import mongoose, { Schema } from 'mongoose';

const TranslationPendingSchema = mongoose.Schema(
  {
    idOwner: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    idTranslation: {
      type: Schema.Types.ObjectId,
      ref: 'Translation'
    },
    version: {
      type: Number,
      default: 0
    },
    new: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Zone'
      }
    ],
    update: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Zone'
      }
    ],
    delete: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Zone'
      }
    ]
  },
  {
    strict: true
  }
);

const TranslationPending = mongoose.model(
  'TranslationPending',
  TranslationPendingSchema
);

export default TranslationPending;
