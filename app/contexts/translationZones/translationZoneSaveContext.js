import { ApiBadRequestError } from 'modules/apiError';

class TranslationZoneSaveContext {
  static async call(zone) {
    try {
      await zone.save();
    } catch (err) {
      throw new ApiBadRequestError(err.message);
    }
    return zone;
  }
}

export default TranslationZoneSaveContext;
