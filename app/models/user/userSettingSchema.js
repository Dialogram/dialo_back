import mongoose from 'mongoose';

const UserSettingSchema = mongoose.Schema(
  {
    confirmedAccount: {
      token: {
        type: String
      },
      confirmed: {
        type: Boolean,
        default: false
      }
    },
    language: {
      type: String,
      enum: ['French', 'English'],
      default: 'English'
    }
  },
  {
    strict: true,
    _id: false
  }
);

export default UserSettingSchema;
