import { ApiBadRequestError } from 'modules/apiError';

class UserUpdateContext {
  static async call(user, givenParams) {
    try {
      await user.update(givenParams);
    } catch (err) {
      throw new ApiBadRequestError(err.message);
    }
    return user;
  }
}

export default UserUpdateContext;
