class PaginationHelper {
  static async setDefaultPagination(query) {
    let pagination = {
      sort: query.sort || { creationDate: -1 },
      page: query.page || 1,
      limit: query.limit || 50,
      offset: query.offset || 0
    };
    return pagination;
  }

  static async parseOption(pagination, req) {
    let options = {
      page: parseInt(pagination.page, 10),
      limit: parseInt(pagination.limit, 10)
    };

    if (req.query.sort) {
      const cleanSorted = req.query.sort.split(',');
      options.sort = { [cleanSorted[0]]: cleanSorted[1] };
    }
    return options;
  }

  static async cleanFilter(query) {
    let filter = query;

    if (query.page || query.limit || query.offset || query.sort) {
      delete filter.page;
      delete filter.limit;
      delete filter.offset;
      delete filter.sort;
    }
    return filter;
  }
}

export default PaginationHelper;
