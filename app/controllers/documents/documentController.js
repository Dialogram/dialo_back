import DocumentSerializer from 'serializers/documents/documentSerializer';
import DocumentCreateContext from 'contexts/documents/documentCreateContext';
import DocumentSaveContext from 'contexts/documents/documentSaveContext';
import DocumentDeleteOneContext from 'contexts/documents/documentDeleteOneContext';
import DocumentHelper from 'helpers/documents/documentHelper';
import PaginationHelper from 'helpers/pagination/paginationHelper';
import DocumentManager from 'modules/documentManager';
import AccessRightManager from 'modules/accessRightManager';
import PaginationSerializer from 'serializers/paginations/paginationSerializer';
import moment from 'moment';

import {
  ApiNotFoundError,
  ApiForbiddenError,
  ApiBadRequestError
} from 'modules/apiError';
import TranslationController from '../translations/translationController';

class DocumentController {
  static async create(req, res, next) {
    let document;
    let translation;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur non renseigné'));
    }
    if (!req.file) {
      return next(new ApiBadRequestError('Fichier incorrect'));
    }
    if (
      !req.body.name ||
      !req.body.description ||
      !req.body.public ||
      !req.body.category
    ) {
      return next(new ApiBadRequestError('Champ(s) manquant(s)'));
    }
    let givenParams = {
      link:
        '/medias/' +
        req.file.filename,
      usageName: req.file.filename,
      name: req.body.name,
      nbPage: await DocumentHelper.getNbPages(req.file.filename),
      idOwner: req.user._id,
      description: req.body.description,
      public: req.body.public,
      category: req.body.category,
      access: {
        idOwner: req.user._id
      }
    };

    try {
      document = await DocumentCreateContext.call(givenParams);
      translation = await TranslationController.createMaster(
        req.user,
        document
      );
      document.idMasterTranslation = translation._id;
      document = await DocumentSaveContext.call(document);
    } catch (err) {
      return next(new ApiBadRequestError('Erreur interne'));
    }
    try {
      document.access['role'] = await AccessRightManager.GetRole(
        document,
        req.user._id
      );
    } catch (err) {
      return next(new ApiBadRequestError(err));
    }

    return res.json({
      data: DocumentSerializer(document)
    });
  }

  static async getById(req, res, next) {
    let document;
    req.io.to('test').emit('FromAPI', { data: 'FROM DOCUMENTCONTROLLER' });

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    try {
      document = await DocumentManager.findByIdAndPopulateComments(
        req.params.idDocument
      );
    } catch (err) {
      return next(new ApiNotFoundError('Document introuvable'));
    }

    try {
      await AccessRightManager.PopulateAccess(document, req.user._id);
    } catch (err) {
      return next(
        new ApiBadRequestError(
          'Erreur pendant la récupération des accès utilisateur' + err
        )
      );
    }

    //Add CDN dynamically from env
    document.link = global.env.server.cdn_pdf + document.link;

    if (document.public == false && document.access.role == 'NONE') {
      return next(new ApiForbiddenError('Accès non autorisé'));
    }
    return res.json({
      data: DocumentSerializer(document)
    });
  }

  static async getMyDocuments(req, res, next) {
    let document;

    // ACCESS TO DO IN PAGINATE
    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    const population = {
      path: 'features.comments',
      populate: {
        path: 'ownerId',
        select: {
          _id: 1,
          nickName: 1,
          'profile.firstName': 1,
          'profile.lastName': 1,
          'profile.certificated': 1,
          'profile.profilePicture': 1
        }
      }
    };
    const pagination = await PaginationHelper.setDefaultPagination(req.query);
    let options = await PaginationHelper.parseOption(pagination, req);
    options.populate = population;
    let filter = await PaginationHelper.cleanFilter(req.query);
    filter.idOwner = req.user._id;
    try {
      document = await DocumentManager.pagination(filter, options);
    } catch (err) {
      return next(err);
    }

    for (let i = 0; document.docs[i]; i++) {
      try {
        await AccessRightManager.PopulateAccess(document.docs[i], req.user._id);
      } catch (err) {
        return next(
          new ApiBadRequestError(
            'Erreur pendant la récupération des accès utilisateur'
          )
        );
      }
      //Add CDN dynamically from env
      document.docs[i].link = global.env.server.cdn_pdf + document.docs[i].link;
    }

    return res.json({
      data: document.docs.map(item => DocumentSerializer(item)),
      includes: [PaginationSerializer(document)]
    });
  }

  static async updateData(req, res, next) {
    let document;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (
      !req.body.name ||
      !req.body.description ||
      !req.body.public ||
      !req.body.category
    ) {
      return next(new ApiBadRequestError('Champ(s) manquant(s)'));
    }
    if (!(document = await DocumentManager.findById(req.params.idDocument))) {
      return next(new ApiNotFoundError('Ressource introuvable'));
    }

    try {
      await AccessRightManager.PopulateAccess(document, req.user._id);
    } catch (err) {
      return next(
        new ApiBadRequestError(
          'Erreur pendant la récupération des accès utilisateur'
        )
      );
    }

    if (
      document.public != true &&
      (document.access.role == 'NONE' || document.access.role == 'collaborator')
    )
      return next(new ApiForbiddenError('Permissions insuffisantes'));

    document.name = req.body.name;
    document.description = req.body.description;
    document.public = req.body.public;
    document.category = req.body.category;
    document.editDate = Date.now();
    document.timestamp = moment();
    try {
      await DocumentSaveContext.call(document);
    } catch (err) {
      return next(err);
    }

    return res.json({
      data: DocumentSerializer(document)
    });
  }

  static async delete(req, res, next) {
    let document;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (!(document = await DocumentManager.findById(req.params.idDocument))) {
      return next(new ApiNotFoundError('Ressource introuvable'));
    }

    try {
      await AccessRightManager.PopulateAccess(document, req.user._id);
    } catch (err) {
      return next(
        new ApiBadRequestError(
          'Erreur pendant la récupération des accès utilisateur'
        )
      );
    }

    if (document.access.role != 'owner') {
      return next(new ApiForbiddenError('Accès non autorisé'));
    }

    try {
      await DocumentDeleteOneContext.call(document, { _id: document._id });
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: {}
    });
  }

  static async getAllDocuments(req, res, next) {
    let document;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    const population = {
      path: 'features.comments',
      populate: {
        path: 'ownerId',
        select: {
          _id: 1,
          nickName: 1,
          'profile.firstName': 1,
          'profile.lastName': 1,
          'profile.certificated': 1,
          'profile.profilePicture': 1
        }
      }
    };

    const pagination = await PaginationHelper.setDefaultPagination(req.query);
    let options = await PaginationHelper.parseOption(pagination, req);
    options.populate = population;
    const filter = await PaginationHelper.cleanFilter(req.query);

    try {
      document = await DocumentManager.pagination(
        filter,
        {
          $or: [
            {
              idOwner: { $eq: req.user._id }
            },
            {
              public: { $eq: true }
            },
            {
              'access.moderator': {
                $elemMatch: { $eq: req.user._id }
              }
            },
            {
              'access.collaborator': {
                $elemMatch: { $eq: req.user._id }
              }
            }
          ]
        },
        options
      );
    } catch (err) {
      return next(err);
    }

    for (let i = 0; i < document.docs.length; i++) {
      try {
        await AccessRightManager.PopulateAccess(document.docs[i], req.user._id);
      } catch (err) {
        return next(
          new ApiBadRequestError(
            'Erreur pendant la récupération des accès utilisateur'
          )
        );
      }
      //Add CDN dynamically from env
      document.docs[i].link = global.env.server.cdn_pdf + document.docs[i].link;
    }

    return res.json({
      data: document.docs.map(item => DocumentSerializer(item)),
      includes: [PaginationSerializer(document)]
    });
  }
}

export default DocumentController;
