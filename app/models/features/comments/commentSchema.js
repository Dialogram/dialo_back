import mongoose, { Schema } from 'mongoose';

const CommentSchema = mongoose.Schema(
  {
    comment: {
      type: String
    },
    likes: [
      {
        type: Schema.Types.ObjectId,
        ref: 'User'
      }
    ],
    creationDate: {
      type: Date,
      default: Date.now
    },
    lastUpdateDate: {
      type: Date,
      default: null
    },
    edited: {
      type: Boolean,
      default: false
    },
    ownerId: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    }
  },
  {
    strict: true
  }
);

const Comment = mongoose.model('Comment', CommentSchema);

export default Comment;
