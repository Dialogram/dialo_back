import expressWinston from 'express-winston';
import winston from 'winston';

const config = {
  levels: {
    emerg: 0,
    alert: 1,
    crit: 2,
    error: 3,
    warning: 4,
    notice: 5,
    info: 6,
    debug: 7
  },

  colors: {
    info: 'green',
    warning: 'yellow',
    error: 'red',
    alert: 'blue' /* Temporary blue because idk what it does represent */,
    crit: 'blue',
    emerg: 'blue',
    notice: 'blue',
    debug: 'blue'
  }
};

winston.addColors(config.colors);

const consoleTransport = new winston.transports.Console();

const logger = winston.createLogger({
  transports: [consoleTransport],
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.simple()
  ),
  levels: config.levels
});

const basicLogger = expressWinston.logger({
  transports: [consoleTransport],
  meta: false,
  expressFormat: true,
  colorize: true,
  winstonInstance: logger
});

export { basicLogger };
