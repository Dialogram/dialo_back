import UserSaveContext from 'contexts/users/userSaveContext';
import UserDeleteContext from 'contexts/users/userDeleteContext';
import UserSerializer from 'serializers/users/userSerializer';
import UserTokenContext from 'contexts/users/userTokenContext';
import UserManager from 'modules/userManager';
import UserHelper from 'helpers/users/userHelper';
import NoReplyMailHelper from 'helpers/users/noreplyMailHelper';

import DocumentDeleteAllContext from 'contexts/documents/documentDeleteAllContext';
import VideoDeleteAllContext from 'contexts/videos/videoDeleteAllContext';

import DocumentManager from 'modules/documentManager';
import VideoManager from 'modules/videoManager';
import TranslationVideoManager from 'modules/translationVideoManager';

import TranslationVideoController from 'controllers/translationVideos/translationVideoController';

import validator from 'email-validator';
import bcrypt from 'bcrypt-nodejs';
import moment from 'moment';

import {
  ApiUnauthorizedError,
  ApiServerError,
  ApiForbiddenError,
  ApiBadRequestError
} from 'modules/apiError';

class SettingsController {
  static async updatePublic(req, res, next) {
    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    req.user.profile.firstName = req.body.profile.firstName;
    req.user.profile.lastName = req.body.profile.lastName;
    req.user.profile.gender = req.body.profile.gender;
    req.user.profile.birthday = req.body.profile.birthday;
    req.user.profile.country = req.body.profile.country;
    req.user.profile.hometown = req.body.profile.hometown;
    req.user.profile.description = req.body.profile.description;
    req.user.timestamp = moment();
    try {
      await UserSaveContext.call(req.user);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: UserSerializer(req.user)
    });
  }

  static async newProfilePicture(req, res, next) {
    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (!req.file) {
      return next(new ApiBadRequestError('Un fichier doit être renseigné'));
    }
    try {
      UserHelper.deleteCloudinaryPicture(
        req.user.profile.profilePicture.public_id
      );
    } catch (err) {
      return next(
        new ApiServerError("Erreur interne avec le service 'cloudinary'")
      );
    }
    req.user.profile.profilePicture.url = req.file.secure_url;
    req.user.profile.profilePicture.public_id = req.file.public_id;
    req.user.timestamp = moment();
    try {
      await UserSaveContext.call(req.user);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: UserSerializer(req.user)
    });
  }

  static async setPassword(req, res, next) {
    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (
      req.user.password != null &&
      req.user.profile.createdWith.provider == 'Dialogram'
    ) {
      return next(new ApiForbiddenError('Le mot de passe est déjà renseigné'));
    }
    req.user.password = req.body.newPassword;
    try {
      await UserSaveContext.call(req.user);
    } catch (err) {
      return next(err);
    }
    NoReplyMailHelper.mailPasswordUpdated(req.user);
    return res.json({
      data: UserSerializer(req.user)
    });
  }

  static async updatePassword(req, res, next) {
    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (req.body.currentPassword && !req.body.newPassword) {
      return next(
        new ApiBadRequestError(
          'Le mot de passe actuel ainsi que le nouveau doivent être renseignés'
        )
      );
    }
    if (!bcrypt.compareSync(req.body.currentPassword, req.user.password)) {
      return next(new ApiForbiddenError('Mot de passe incorrect'));
    }
    req.user.password = req.body.newPassword;
    try {
      await UserSaveContext.call(req.user);
    } catch (err) {
      return next(err);
    }
    NoReplyMailHelper.mailPasswordUpdated(req.user);
    return res.json({
      data: UserSerializer(req.user)
    });
  }

  static async updateAccount(req, res, next) {
    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (!req.body.email || !req.body.nickName) {
      return next(
        new ApiBadRequestError(
          "L'adresse courriel ainsi que le pseudonyme doivent être renseignés"
        )
      );
    }
    if (validator.validate(req.body.email) == false) {
      return next(new ApiBadRequestError("L'adresse courriel est invalide"));
    }
    try {
      if (req.user.nickName != req.body.nickName) {
        req.user = await UserHelper.updateNickname(req);
      }
      if (req.user.email != req.body.email) {
        req.user = await UserHelper.updateEmail(req);
      }
    } catch (err) {
      return next(err);
    }
    try {
      await UserSaveContext.call(req.user);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: UserSerializer(req.user)
    });
  }

  static async confirmUpdatedEmail(req, res, next) {
    let user = await UserManager.findByTokenUpdateEmail(req);

    if (!user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    user.email = user.tokenReset.resetEmail.updateEmail;
    user.tokenReset.resetEmail.tokenEmail = null;
    user.tokenReset.resetEmail.updateEmail = null;
    user.timestamp = moment();
    try {
      await UserSaveContext.call(user);
    } catch (err) {
      return next(err);
    }
    NoReplyMailHelper.mailEmailUpdated(user);
    return res.json({
      data: UserSerializer(req.user)
    });
  }

  static async sendMailConfirmAccount(req, res, next) {
    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (req.user.settings.confirmedAccount.confirmed == true) {
      return next(
        new ApiForbiddenError("L'utilisateur a déjà confirmé son compte")
      );
    }
    try {
      req.user = await UserTokenContext.createConfirmAccountToken(
        req.user,
        '1d'
      );
      await UserSaveContext.call(req.user);
      NoReplyMailHelper.mailConfirmAccount(req.user);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: {}
    });
  }

  static async confirmAccount(req, res, next) {
    let user = await UserManager.findByTokenConfirmAccount(req);

    if (!user) {
      return next(
        new ApiForbiddenError(
          "Utilisateur introuvable ou jeton d'authentification expiré"
        )
      );
    }
    user.settings.confirmedAccount.confirmed = true;
    user.settings.confirmedAccount.token = null;
    NoReplyMailHelper.mailCreateUser(user);
    try {
      await UserSaveContext.call(user);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: UserSerializer(user)
    });
  }

  static async deleteMe(req, res, next) {
    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    let document = await DocumentManager.findByOwner(req.user._id);

    try {
      if (document.length > 0) {
        await DocumentDeleteAllContext.call(document, {
          idOwner: req.user._id
        });
      }
      await UserHelper.deleteCloudinaryPicture(
        req.user.profile.profilePicture.public_id
      );
      await UserDeleteContext.call(req.user, { _id: req.user._id });
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: {}
    });
  }
}

export default SettingsController;
