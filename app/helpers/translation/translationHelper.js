import TranslationZoneManager from 'modules/translationZoneManager';

class TranslationHelper {
  static async isSameZone(masterZone, zone) {
    if (
      masterZone.x != zone.x ||
      masterZone.y != zone.y ||
      masterZone.width != zone.width ||
      masterZone.height != zone.height ||
      masterZone.page != zone.page ||
      masterZone.idVideo != zone.idVideo
    ) {
      return false;
    }
    return true;
  }

  static async compareMasterTranslation(translation) {
    let pendingTranslation = {
      version: 0,
      new: [],
      update: [],
      delete: []
    };
    let masterZone;

    for (let i = 0; i < translation.zones.length; i++) {
      {
        switch (translation.zones[i].state) {
          case 'MASTER':
            if (
              !(masterZone = TranslationZoneManager.findById(
                translation.zones[i]._id
              ))
            ) {
              pendingTranslation.new.push(translation.zones[i]);
              break;
            }
            if (this.isSameZone(masterZone, translation.zones[i])) {
              break;
            }
            pendingTranslation.update.push(translation.zones[i]);
            break;
          case 'MASTER_UPDATE':
            pendingTranslation.update.push(translation.zones[i]);
            break;
          case 'NEW':
            pendingTranslation.new.push(translation.zones[i]);
            break;
          case 'DELETED':
            pendingTranslation.delete.push(translation.zones[i]);
            break;
        }
      }
    }
    return pendingTranslation;
  }

  static async pullMasterTranslation(translation) {
    let dataPulled = {
      version: 0,
      new: [],
      update: [],
      delete: []
    };
    let masterZone;

    for (let i = 0; i < translation.zones.length; i++) {
      {
        switch (translation.zones[i].state) {
          case 'MASTER':
            if (
              !(masterZone = TranslationZoneManager.findById(
                translation.zones[i].idMaster
              ))
            ) {
              dataPulled.new.push(translation.zones[i]);
              break;
            }
            if (this.isSameZone(masterZone, translation.zones[i])) {
              break;
            }
            dataPulled.update.push(translation.zones[i]);
            break;
          case 'MASTER_UPDATE':
            dataPulled.update.push(translation.zones[i]);
            break;
          case 'NEW':
            dataPulled.new.push(translation.zones[i]);
            break;
          case 'DELETED':
            dataPulled.delete.push(translation.zones[i]);
            break;
        }
      }
    }
    return dataPulled;
  }
}

export default TranslationHelper;
