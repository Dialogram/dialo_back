import TranslationVideo from 'models/translationVideos/translationVideoSchema';
import { ApiBadRequestError } from 'modules/apiError';

class TranslationVideoCreateContext {
  static async call(req, video) {
    let givenParams = {
      title: req.body.title,
      idApiVideo: video.videoId,
      description: video.description,
      public: req.body.public,
      idOwner: req.user._id,
      publishedAt: video.publishedAt,
      tags: video.tags,
      metadata: video.metadata,
      source: {
        type: video.source.type,
        uri: video.source.uri
      },
      assets: {
        iframe: video.assets.iframe,
        player: video.assets.player,
        hls: video.assets.hls,
        thumbnail: video.assets.thumbnail
      }
    };
    const createdVideo = new TranslationVideo(givenParams);
    try {
      await createdVideo.save();
    } catch (err) {
      throw new ApiBadRequestError(err.message);
    }
    return createdVideo;
  }
}

export default TranslationVideoCreateContext;
