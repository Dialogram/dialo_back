import { ApiServerError } from 'modules/apiError';

class UserDeleteContext {
  static async call(user, querry) {
    await user.remove(querry, err => {
      if (err) {
        throw new ApiServerError(err.message);
      }
    });
  }
}

export default UserDeleteContext;
