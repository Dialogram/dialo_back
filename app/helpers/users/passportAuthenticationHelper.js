import UserHelper from 'helpers/users/userHelper';

class PassportAuthenticationHelper {
  static getFacebookUserInfo(fbUser) {
    let user;

    user = {
      email: fbUser._json.email,
      nickName: UserHelper.createNickname(
        fbUser._json.first_name,
        fbUser._json.last_name
      ),
      profile: {
        createdWith: {
          provider: 'Facebook',
          userId: fbUser.id
        },
        firstName: null,
        lastName: null,
        profilePicture: {
          url: null,
          public_id: null
        },
        birthday: null,
        gender: null,
        hometown: null
      }
    };
    if (fbUser._json.hometown) {
      user.profile.hometown = fbUser._json.hometown;
    }
    if (fbUser._json.gender) {
      user.profile.gender = fbUser._json.gender;
    }
    if (fbUser._json.birthday) {
      user.profile.birthday = fbUser._json.birthday;
    }
    if (fbUser.photos[0].value) {
      user.profile.profilePicture.url = fbUser.photos[0].value;
    }
    if (fbUser._json.first_name) {
      user.profile.firstName = fbUser._json.first_name;
    }
    if (fbUser._json.last_name) {
      user.profile.lastName = fbUser._json.last_name;
    }
    return user;
  }

  static getGoogleUserInfo(googleUser) {
    let user;

    user = {
      email: googleUser.emails[0].value,
      nickName: UserHelper.createNickname(
        googleUser.name.givenName,
        googleUser.name.familyName
      ),
      profile: {
        createdWith: {
          provider: 'Google',
          userId: googleUser.id
        },
        firstName: null,
        lastName: null,
        profilePicture: {
          url: null,
          public_id: null
        },
        birthday: null,
        gender: null,
        hometown: null
      }
    };
    /*        if (googleUser._json.hometown) { user.profile.hometown = googleUser._json.hometown }
        if (googleUser._json.gender) { user.profile.gender = googleUser._json.gender }
        if (googleUser._json.birthday) { user.profile.birthday = googleUser._json.birthday }*/
    if (googleUser.photos[0].value) {
      user.profile.profilePicture.url = googleUser.photos[0].value + '?sz=400';
    }
    if (googleUser.name.givenName) {
      user.profile.firstName = googleUser.name.givenName;
    }
    if (googleUser.name.familyName) {
      user.profile.lastName = googleUser.name.familyName;
    }
    return user;
  }
}

export default PassportAuthenticationHelper;
