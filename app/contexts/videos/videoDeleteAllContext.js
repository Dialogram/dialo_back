import { ApiServerError } from 'modules/apiError';

class VideoDeleteAllContext {
  static async call(video, query) {
    let i = 0;

    while (video[i]) {
      await video[i].remove(query, err => {
        if (err) {
          throw new ApiServerError(err.message);
        }
      });
      i++;
    }
  }
}

export default VideoDeleteAllContext;
