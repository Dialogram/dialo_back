import { ApiBadRequestError } from 'modules/apiError';

class TicketResponseCreateContext {
  static async call(req, ticket) {
    let givenParams = req.body;

    if (!givenParams.content) {
      throw new ApiBadRequestError('Le content doit etre renseigné');
    }

    return this.createResponse(givenParams.content, ticket, req);
  }

  static async createResponse(content, ticket, req) {
    let response = {
      content,
      idOwner: req.user._id,
      emailOwner: req.user.email
    };

    if (ticket.responses instanceof Array) {
      ticket.responses.push(response);
    }

    try {
      await ticket.save();
    } catch (err) {
      throw new ApiBadRequestError(err.message);
    }
    return ticket;
  }
}

export default TicketResponseCreateContext;
