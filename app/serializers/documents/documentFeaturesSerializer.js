import CommentSerializer from 'serializers/features/comments/commentSerializer';

const documentFeaturesSerializer = features => {
  if (!features) {
    return {};
  }

  const data = {
    likes: features.likes,
    favorites: features.favorites,
    comments: features.comments.map(item => CommentSerializer(item))
  };
  return data;
};

export default documentFeaturesSerializer;
