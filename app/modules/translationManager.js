import Translation from 'models/translations/translationSchema';

class TranslationManager {
  static async findById(idTranslation) {
    return await Translation.findById(idTranslation);
  }

  static async findOneByIdDocument(idDocument) {
    return await Translation.findOne({ idDocument: idDocument });
  }

  static async findOneMasterByIdDocument(idDocument) {
    return await Translation.findOne({
      idDocument: idDocument,
      isMaster: true
    });
  }

  static async findOneMasterByIdTranslationAndPopulateZone(
    idMasterTranslation
  ) {
    let translation;
    let arr = [];

    if (
      !(translation = await Translation.findOne({
        id: idMasterTranslation,
        isMaster: true
      }))
    ) {
      return;
    }
    for (let i = 0; i < translation.zones.length; i++) {
      arr.push(`zones.${i} `);
    }
    return await Translation.findOne({
      id: idMasterTranslation,
      isMaster: true
    }).populate({
      path: arr.join(''),
      model: 'Zone'
    });
  }

  static async findOneMasterByIdDocumentAndPopulateZone(idDocument) {
    let translation;
    let arr = [];

    if (
      !(translation = await Translation.findOne({
        idDocument: idDocument,
        isMaster: true
      }))
    ) {
      return;
    }
    for (let i = 0; i < translation.zones.length; i++) {
      arr.push(`zones.${i} `);
    }
    return await Translation.findOne({
      idDocument: idDocument,
      isMaster: true
    }).populate({
      path: arr.join(''),
      model: 'Zone'
    });
  }

  static async findByIdAndPopulateVideos(idTranslation) {
    let translation;
    let arr = [];

    if (!(translation = await Translation.findById(idTranslation))) {
      return;
    }
    for (let i = 0; i < translation.zones.length; i++) {
      arr.push(`zones.${i}.idVideo `);
    }
    return await Translation.findById(idTranslation).populate({
      path: arr.join(''),
      model: 'TranslationVideo',
      select: {
        _id: 1,
        title: 1,
        idOwner: 1,
        'assets.hls': 1
      }
    });
  }

  static async findByIdAndPopulateZonesPage(idTranslation, pageNumber) {
    let translation;
    let arr = [];

    if (!(translation = await Translation.findById(idTranslation))) {
      return;
    }
    for (let i = 0; i < translation.zones.length; i++) {
      arr.push(`zones.${i} `);
    }
    return await Translation.findById(idTranslation).populate({
      path: arr.join(''),
      model: 'Zone',
      match: { page: `${pageNumber}` }
    });
  }

  static async findByIdAndPopulateZones(idTranslation) {
    let translation;
    let arr = [];

    if (!(translation = await Translation.findById(idTranslation))) {
      return;
    }
    for (let i = 0; i < translation.zones.length; i++) {
      arr.push(`zones.${i} `);
    }
    return await Translation.findById(idTranslation).populate({
      path: arr.join(''),
      model: 'Zone'
    });
  }

  static async findByIdDocument(idDocument) {
    return await Translation.find({ idDocument: idDocument });
  }

  static async findZoneById(translation, idZone) {
    let zone;

    if (
      !(zone = await Translation.findOne({
        _id: translation._id,
        zones: [`${idZone}`]
      }))
    ) {
      return;
    }
    return zone;
  }
}

export default TranslationManager;
