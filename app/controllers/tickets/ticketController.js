import TicketCreateContext from 'contexts/tickets/ticketCreateContext';
import TicketResponseCreateContext from 'contexts/tickets/ticketResponseCreateContext';

import TicketSaveContext from 'contexts/tickets/ticketSaveContext';
import TicketManager from 'modules/ticketManager';
import TicketSerializer from 'serializers/tickets/ticketSerializer';
import moment from 'moment';
import UserManager from 'modules/userManager';
import {
  ApiNotFoundError,
  ApiForbiddenError,
  ApiBadRequestError
} from 'modules/apiError';

class TicketController {
  static async create(req, res, next) {
    let ticket;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (!req.body.category) {
      return next(new ApiBadRequestError('Une catégorie doit être renseignée'));
    }
    if (req.body.title.length < 5 || req.body.title.length > 80) {
      return next(
        new ApiForbiddenError(
          'La taille du titre doit être comprise entre 5 et 80 caractères'
        )
      );
    }
    if (req.body.description.length < 100) {
      return next(
        new ApiForbiddenError(
          'La taille de la description doit être supèrieure à 100 caractères'
        )
      );
    }
    let givenParams = {
      title: req.body.title,
      category: req.body.category,
      description: req.body.description,
      idOwner: req.user._id
    };
    try {
      ticket = await TicketCreateContext.call(givenParams);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: TicketSerializer(ticket)
    });
  }

  static async changeTicketStatus(req, res, next) {
    let ticket;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    try {
      ticket = await TicketManager.findById(req.params.idTicket);
    } catch (e) {
      return next(new ApiNotFoundError('Ressource introuvable'));
    }
    if (!ticket) {
      return next(new ApiForbiddenError('Le ticket est introuvable'));
    }

    if (
      req.params.status === 'broadcast' ||
      req.params.status === 'open' ||
      req.params.status === 'in progress' ||
      req.params.status === 'close'
    ) {
      ticket.status = req.params.status;
    } else {
      return next(new ApiForbiddenError('le status est inconnu'));
    }
    try {
      await TicketSaveContext.call(ticket);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: TicketSerializer(ticket)
    });
  }
  static async closeTicket(req, res, next) {
    let ticket;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    try {
      ticket = await TicketManager.findById(req.params.idTicket);
    } catch (err) {
      return next(new ApiNotFoundError('Ressource introuvable'));
    }
    if (ticket.status == 'close') {
      return next(new ApiForbiddenError('Le ticket est déjà clos'));
    }
    ticket.status = 'close';
    ticket.lastUpdateDate = Date.now();
    ticket.closedDate = Date.now();
    ticket.timestamp = moment();
    try {
      await TicketSaveContext.call(ticket);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: TicketSerializer(ticket)
    });
  }

  static async getById(req, res, next) {
    let tickets;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    try {
      tickets = await TicketManager.findById(req.params.idTicket);
    } catch (err) {
      return next(new ApiNotFoundError('Ticket introuvable'));
    }
    return res.json({
      data: TicketSerializer(tickets)
    });
  }

  static async getByCategory(req, res, next) {
    let tickets;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    try {
      tickets = await TicketManager.findByCategory(req);
    } catch (err) {
      return next(new ApiNotFoundError('Ticket introuvable'));
    }
    return res.json({
      data: tickets.map(item => TicketSerializer(item)),
      includes: []
    });
  }

  static async getByStatus(req, res, next) {
    let tickets;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    try {
      tickets = await TicketManager.findByStatus(req);
    } catch (err) {
      return next(new ApiNotFoundError('Ticket introuvable'));
    }
    return res.json({
      data: tickets.map(item => TicketSerializer(item)),
      includes: []
    });
  }

  static async getForAuthenticatedUser(req, res, next) {
    let tickets;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    try {
      tickets = await TicketManager.findByUser(req.user._id);
    } catch (err) {
      return next(new ApiNotFoundError('Ticket introuvable'));
    }
    return res.json({
      data: tickets.map(item => TicketSerializer(item)),
      includes: []
    });
  }

  static async getByUser(req, res, next) {
    let tickets;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    try {
      tickets = await TicketManager.findByUser(req.params.userId);
    } catch (err) {
      return next(new ApiNotFoundError('Ticket introuvable'));
    }
    return res.json({
      data: tickets.map(item => TicketSerializer(item)),
      includes: []
    });
  }

  static async getAll(req, res, next) {
    let tickets;
    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    try {
      tickets = await TicketManager.findOpenTicket();
    } catch (e) {
      console.log(e);
      return next(new ApiNotFoundError('Tickets introuvable'));
    }
    return res.json({
      data: tickets.map(item => TicketSerializer(item)),
      includes: []
    });
  }

  static async postNewAnswer(req, res, next) {
    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    let tickets;
    try {
      tickets = await TicketManager.findById(req.params.ticketId);
    } catch (e) {
      return next(new ApiNotFoundError('Tickets introuvable'));
    }
    if (tickets === null) {
      return next(new ApiNotFoundError('Tickets introuvable'));
    }

    if (req.user.role !== 'admin' && tickets.idOwner !== req.user._id) {
      return next(
        new ApiForbiddenError(
          "Vous 'avez pas les droits pour répondre à ce ticket"
        )
      );
    }
    if (
      tickets.responses.length > 0 &&
      tickets.responses[tickets.responses.length - 1].idOwner.toString() ===
        `${req.user._id}`
    ) {
      return next(
        new ApiForbiddenError(
          'Veuillez attendre une réponse avant de pouvoir répondre.'
        )
      );
    }
    tickets = await TicketResponseCreateContext.call(req, tickets);
    return res.json({
      data: TicketSerializer(tickets)
    });
  }

  static async getByUserEmail(req, res, next) {
    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    let focusedUser;
    try {
      focusedUser = await UserManager.findByEmail(req.params.email);
    } catch (e) {
      return next(new ApiNotFoundError('No user found'));
    }
    let tickets;
    try {
      tickets = await TicketManager.findByUser(focusedUser._id);
    } catch (e) {
      return next(new ApiNotFoundError('Tickets introuvable'));
    }
    return res.json({
      data: tickets.map(item => TicketSerializer(item)),
      includes: []
    });
  }
}

export default TicketController;
