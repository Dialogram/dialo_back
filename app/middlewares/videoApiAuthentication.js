import multer from 'multer';

import { ApiBadRequestError } from 'modules/apiError';

class VideoApiAuthentication {
  static configureFilename() {
    const storage = multer.diskStorage({
      filename: (req, file, done) => {
        return done(null, Date.now() + '-' + file.originalname);
      }
    });
    return storage;
  }

  static configureFileFormat() {
    const fileFilter = (req, file, done) => {
      if (
        file.mimetype != 'video/mp4' &&
        file.mimetype != 'video/avi' &&
        file.mimetype != 'video/mov' &&
        file.mimetype != 'video/flv' &&
        file.mimetype != 'video/wmv'
      ) {
        return done(new ApiBadRequestError('Wrong format file'));
      }
      return done(null, true);
    };
    return fileFilter;
  }

  static uploadVideo() {
    return multer({
      storage: this.configureFilename(),
      fileFilter: this.configureFileFormat()
    }).single('video');
  }
}

export default VideoApiAuthentication;
