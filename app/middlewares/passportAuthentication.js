import passport from 'passport';
import FacebookStrategy from 'passport-facebook';
import GoogleStrategy from 'passport-google-oauth20';
import UserManager from 'modules/userManager';
import PassportAuthenticationHelper from 'helpers/users/passportAuthenticationHelper';

import { ApiNotFoundError } from 'modules/apiError';

class PassportAuthentication {
  static facebookInitialize() {
    passport.use(
      new FacebookStrategy(
        {
          clientID: global.env.facebook_auth.id,
          clientSecret: global.env.facebook_auth.secret,
          callbackURL: `${global.env.server.http}://${global.env.server.url}/api/auth/facebook/callback`,
          profileFields: [
            'email',
            'name',
            'hometown',
            'gender',
            'birthday',
            'age_range',
            'photos'
          ],
          scope: [
            'email',
            'user_birthday',
            'user_hometown',
            'user_location',
            'user_photos',
            'user_gender',
            'user_age_range',
            'public_profile'
          ]
        },
        async (accessToken, refreshToken, fbUser, done) => {
          let user;

          if (fbUser == null) {
            return next(new ApiNotFoundError('Utilisateur introuvable'));
          }
          if (
            (user = await UserManager.findByProvider('Facebook', fbUser.id))
          ) {
            return done(null, user);
          }
          user = PassportAuthenticationHelper.getFacebookUserInfo(fbUser);
          return done(null, user);
        }
      )
    );
  }

  static googleInitialize() {
    passport.use(
      new GoogleStrategy(
        {
          clientID: global.env.google_auth.id,
          clientSecret: global.env.google_auth.secret,
          callbackURL: `${global.env.server.http}://${global.env.server.url}/api/auth/google/callback`,
          scope: [
            'https://www.googleapis.com/auth/youtube.upload',
            'https://www.googleapis.com/auth/youtube.force-ssl',
            'https://www.googleapis.com/auth/youtube.readonly',
            'https://www.googleapis.com/auth/userinfo.profile',
            'openid'
          ]
        },
        async (accessToken, refreshToken, googleUser, done) => {
          let user;

          if (googleUser == null) {
            return next(new ApiNotFoundError('Utilisateur introuvable'));
          }
          if (
            (user = await UserManager.findByProvider('Google', googleUser.id))
          ) {
            return done(null, user); // User already exist
          }
          user = PassportAuthenticationHelper.getGoogleUserInfo(googleUser);
          return done(null, user);
        }
      )
    );
  }
}

export default PassportAuthentication;
