import nodemailer from 'nodemailer';

import 'dotenv/config';

class NoReplyMailHelper {
  static mailCreateUser(user) {
    var smtpTransport = nodemailer.createTransport({
      service: 'Gmail',
      auth: {
        type: 'login',
        user: global.env.gmail.gmailAccount,
        pass: global.env.gmail.gmailSecret
      }
    });
    var mailOptions = {
      to: user.email,
      from: global.env.gmail.gmailAccount,
      subject: "Bienvenue sur Dialogram! Votre compte vient d'être crée.",
      text:
        'Bonjour ' +
        user.profile.firstName +
        ',\n' +
        'Bienvenue sur Dialogram.\n' +
        "Votre compte vient d'être crée et confirmé avec succés.\n" +
        "Vous trouverez ci-dessous vos informations d'authentification:\n" +
        "Nom d'utilisateur: " +
        user.nickName +
        '\n' +
        'Vous pouvez utiliser ces informations pour vous connecter aux services et applications de Dialogram.\n' +
        'Nous vous invitions également à ne jamais communiquer vos informations personnelles.\n\n' +
        "Cordialement, l'équipe Dialogram."
    };
    smtpTransport.sendMail(mailOptions);
  }

  static mailConfirmAccount(user) {
    var smtpTransport = nodemailer.createTransport({
      service: 'Gmail',
      auth: {
        type: 'login',
        user: global.env.gmail.gmailAccount,
        pass: global.env.gmail.gmailSecret
      }
    });
    var mailOptions = {
      to: user.email,
      from: global.env.gmail.gmailAccount,
      subject: 'Vérifiez votre adresse mail',
      text:
        'Bonjour ' +
        user.profile.firstName +
        ',\n' +
        'Bienvenue sur Dialogram.\n' +
        'Veuillez cliquez sur le lien de confirmation ci-dessous afin de terminer votre inscription.\n' +
        'Activer votre compte: ' +
        global.env.server.http +
        '://' +
        global.env.app.url +
        '/account/confirm/' +
        user.settings.confirmedAccount.token +
        '\n' +
        "Vous n'avez pas crée de compte sur Dialogram? Quelqu'un a peut-être malencontreusement entré votre adresse mail; vous pouvez ignorer ce message dans ce cas.\n" +
        "Cordialement, l'équipe Dialogram."
    };
    smtpTransport.sendMail(mailOptions);
  }

  static mailForgotPasswordReset(user, token) {
    var smtpTransport = nodemailer.createTransport({
      service: 'Gmail',
      auth: {
        type: 'login',
        user: global.env.gmail.gmailAccount,
        pass: global.env.gmail.gmailSecret
      }
    });
    var mailOptions = {
      to: user.email,
      from: global.env.gmail.gmailAccount,
      subject: 'Réinitialiser votre mot de passe Dialogram',
      text:
        "Vous avez reçu ce courrier car vous (ou quelqu'un d'autre) avez lancé la procédure de réinitialisation de mot de passe.\n\n" +
        'Veuillez cliquer sur le lien ci-dessous pour modifier votre mot de passe, vous pouvez également copier/coller ce lien dans votre navigateur pour accéder à la page:\n\n' +
        global.env.server.http +
        '://' +
        global.env.app.url +
        '/password/forget/' +
        token +
        '\n\n' +
        "Si vous n'êtes pas à l'origine de cette requête, veuillez ignorer ce message, votre de mot de passe restera inchangé.\n" +
        "Cordialement, l'équipe Dialogram."
    };
    smtpTransport.sendMail(mailOptions);
  }

  static mailPasswordUpdated(user) {
    var smtpTransport = nodemailer.createTransport({
      service: 'Gmail',
      auth: {
        type: 'login',
        user: global.env.gmail.gmailAccount,
        pass: global.env.gmail.gmailSecret
      }
    });
    var mailOptions = {
      to: user.email,
      from: global.env.gmail.gmailAccount,
      subject: 'Le mot de passe de votre compte Dialogram a été modifié',
      text:
        'Bonjour ' +
        user.nickName +
        ', ce courrier vous informe que votre mot de passe lié au compte ' +
        user.email +
        ' a été modifié avec succés.\n' +
        "Cordialement, l'équipe Dialogram."
    };
    smtpTransport.sendMail(mailOptions);
  }

  static mailInformEmailUpdate(user, newEmail) {
    var smtpTransport = nodemailer.createTransport({
      service: 'Gmail',
      auth: {
        type: 'login',
        user: global.env.gmail.gmailAccount,
        pass: global.env.gmail.gmailSecret
      }
    });
    var mailOptions = {
      to: user.email,
      from: global.env.gmail.gmailAccount,
      subject:
        "L'adresse mail de l'utilisateur " + user.nickName + ' a été modifiée',
      text:
        'Bonjour ' +
        user.nickName +
        ', votre adresse mail a été modifiée avec succés.\n' +
        "Pour confirmer la nouvelle adresse de l'utilisateur " +
        user.nickName +
        ", suivez le lien envoyé sur l'adresse " +
        newEmail +
        ". Si vous n'êtes pas à l'origine de cette requête, veuillez contacter le supprot Dialogram immédiatement." +
        "Cordialement, l'équipe Dialogram."
    };
    smtpTransport.sendMail(mailOptions);
  }

  static mailConfirmEmailUpdate(user, newEmail) {
    var smtpTransport = nodemailer.createTransport({
      service: 'Gmail',
      auth: {
        type: 'login',
        user: global.env.gmail.gmailAccount,
        pass: global.env.gmail.gmailSecret
      }
    });
    var mailOptions = {
      to: newEmail,
      from: global.env.gmail.gmailAccount,
      subject: 'Confirmation de votre compte, ' + user.nickName + '.',
      text:
        'Bonjour ' +
        user.nickName +
        ', \n' +
        "Veuillez confirmer votre compte lié à l'utilisateur " +
        user.nickName +
        ". C'est facile, il vous suffit de cliquer sur le lien ci-dessous.\n" +
        global.env.server.http +
        '://' +
        global.env.app.url +
        '/api/user/account/email/' +
        user.tokenReset.resetEmail.tokenEmail +
        '\n\n' +
        "Si vous n'êtes pas à l'origine de cette requête, veuillez ignorer ce message, votre adresse mail restera inchangé.\n" +
        "Cordialement, l'équipe Dialogram."
    };
    smtpTransport.sendMail(mailOptions);
  }

  static mailEmailUpdated(user) {
    var smtpTransport = nodemailer.createTransport({
      service: 'Gmail',
      auth: {
        type: 'login',
        user: global.env.gmail.gmailAccount,
        pass: global.env.gmail.gmailSecret
      }
    });
    var mailOptions = {
      to: user.email,
      from: global.env.gmail.gmailAccount,
      subject: 'Votre mot de passe Dialogram a été modifié',
      text:
        'Bonjour ' +
        user.nickName +
        ', ce courrier vous indique que la modification de votre mot de passe pour le compte ' +
        user.email +
        ' a été effectuée avec succés.\n' +
        "Si vous n'êtes pas à l'origine de cette requête, veuillez contacter le supprot Dialogram immédiatement." +
        "Cordialement, l'équipe Dialogram."
    };
    smtpTransport.sendMail(mailOptions);
  }
}

export default NoReplyMailHelper;
