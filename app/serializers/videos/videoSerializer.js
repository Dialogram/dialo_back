const videoSerializer = video => {
  if (!video) {
    return {};
  }

  const data = {
    type: 'videos',
    id: video.id,
    idOwner: video.idOwner,
    idApiVideo: video.idApiVideo,
    title: video.title,
    description: video.description,
    public: video.public,
    category: video.category,
    publishedAt: video.publishedAt,
    tags: video.tags,
    metadata: video.metadata,
    source: {
      type: video.source.type,
      uri: video.source.uri
    },
    assets: {
      iframe: video.assets.iframe,
      player: video.assets.player,
      hls: video.assets.hls,
      thumbnail: video.assets.thumbnail
    },
    timestamp: video.timestamp,
    access: {
      role: video.access.role,
      moderators: video.access.moderator,
      collaborator: video.access.collaborator
    }
  };
  return data;
};

export default videoSerializer;
