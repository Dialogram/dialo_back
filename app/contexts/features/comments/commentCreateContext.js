import Comment from 'models/features/comments/commentSchema';
import { ApiBadRequestError } from 'modules/apiError';

class CommentCreateContext {
  static async call(req) {
    let comment;

    comment = new Comment(req);
    try {
      await comment.save();
    } catch (err) {
      throw new ApiBadRequestError(err.message);
    }
    return comment;
  }
}

export default CommentCreateContext;
