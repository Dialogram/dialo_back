const ticketSerializer = ticket => {
  if (!ticket) {
    return {};
  }

  const data = {
    type: 'ticket',

    id: ticket.id,
    title: ticket.title,
    category: ticket.category,
    status: ticket.status,
    description: ticket.description,
    idOwner: ticket.idOwner,
    creationDate: ticket.creationDate,
    lastUpdateDate: ticket.lastUpdateDate,
    closedDate: ticket.closedDate,
    timestamp: ticket.timestamp,
    responses: ticket.responses
  };
  return data;
};

export default ticketSerializer;
