import mongoose from 'mongoose';

const UserTokenResetSchema = mongoose.Schema(
  {
    tokenPassword: {
      type: String
    },
    resetEmail: {
      tokenEmail: {
        type: String
      },
      updateEmail: {
        type: String,
        match: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
      }
    }
  },
  {
    strict: true,
    _id: false
  }
);

export default UserTokenResetSchema;
