import mongoose, { Schema } from 'mongoose';
import moment from 'moment';

const TranslationSchema = mongoose.Schema(
  {
    idDocument: {
      type: Schema.Types.ObjectId,
      ref: 'Document'
    },
    idOwner: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    description: {
      type: String,
      default: null
    },
    zones: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Zone'
      }
    ],
    version: {
      type: Number,
      default: 0
    },
    isMaster: {
      type: Boolean,
      default: false
    },
    timestamp: {
      type: Number,
      default: moment().unix(1318874398806)
    },
    certificated: {
      type: Boolean,
      default: false
    },
    isForked: {
      type: Boolean,
      default: false
    },
    idMasterTranslation: {
      type: Schema.Types.ObjectId,
      ref: 'Translation',
      default: null
    },
    forkedTranslations: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Translation'
      }
    ],
    pushed: {
      type: Boolean,
      default: false
    }
  },
  {
    strict: true
  }
);

const Translation = mongoose.model('Translation', TranslationSchema);

export default Translation;
