import mongoose from 'mongoose';
import Schema from 'mongoose';
import moment from 'moment';
import mongoosePaginate from 'mongoose-paginate-v2';
import accessRightSchema from 'models/access_rights/accessRightsSchema';

const VideoSchema = mongoose.Schema(
  {
    idOwner: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    idApiVideo: {
      type: String,
      required: true
    },
    title: {
      type: String,
      required: true
    },
    description: {
      type: String,
      default: null
    },
    public: {
      type: Boolean,
      default: false
    },
    category: {
      type: String,
      enum: [
        'health',
        'finance',
        'administrative',
        'entertainment',
        'business'
      ],
      default: 'entertainment',
      searchable: true
    },
    publishedAt: {
      type: String,
      default: Date.now()
    },
    tags: {
      type: [String],
      default: []
    },
    metadata: {
      type: [String],
      default: []
    },
    source: {
      type: {
        type: String,
        required: true
      },
      uri: {
        type: String,
        required: true
      }
    },
    assets: {
      iframe: {
        type: String,
        required: true
      },
      player: {
        type: String,
        required: true
      },
      hls: {
        type: String,
        required: true
      },
      thumbnail: {
        type: String,
        required: true
      }
    },
    timestamp: {
      type: Number,
      default: moment().unix(1318874398806)
    },
    access: {
      type: accessRightSchema,
      default: {}
    }
  },
  {
    strict: true
  }
);

VideoSchema.plugin(mongoosePaginate);
const Video = mongoose.model('Video', VideoSchema);

export default Video;
