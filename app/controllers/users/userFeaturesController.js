import UserSaveContext from 'contexts/users/userSaveContext';
import UserSerializer from 'serializers/users/userSerializer';
import UserManager from 'modules/userManager';

import moment from 'moment';

import {
  ApiUnauthorizedError,
  ApiForbiddenError,
  ApiBadRequestError
} from 'modules/apiError';

class UserFeaturesController {
  static async follow(req, res, next) {
    let followedUser;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (!req.body.nickName) {
      return next(
        new ApiBadRequestError(
          "Le pseudonyme de l'utilisateur doit être renseigné"
        )
      );
    }
    if (
      (followedUser = await UserManager.findByNickName(req.body.nickName)) ==
      null
    ) {
      return next(
        new ApiForbiddenError("Le pseudonyme de l'utilisateur est introuvable")
      );
    }
    if (followedUser.nickName == req.user.nickName) {
      return next(
        new ApiForbiddenError('Vous ne pouvez pas vous abonner à vous même')
      );
    }
    for (let i = 0; i < req.user.features.follows.length; i++) {
      if (req.user.features.follows[i]._id.equals(followedUser._id)) {
        return next(new ApiForbiddenError('Vous suivez déjà cet utilisateur'));
      }
    }
    req.user.features.follows.push(followedUser);
    followedUser.features.followers.push(req.user);
    req.user.timestamp = moment();
    followedUser.timestamp = moment();
    try {
      await UserSaveContext.call(followedUser);
      await UserSaveContext.call(req.user);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: UserSerializer(req.user)
    });
  }

  static async unfollow(req, res, next) {
    let unfollowedUser;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (!req.body.nickName) {
      return next(
        new ApiBadRequestError(
          "Le pseudonyme de l'utilisateur doit être renseigné"
        )
      );
    }
    if (
      (unfollowedUser = await UserManager.findByNickName(req.body.nickName)) ==
      null
    ) {
      return next(
        new ApiForbiddenError("Le pseudonyme de l'utilisateur est introuvable")
      );
    }
    if (unfollowedUser.nickName == req.user.nickName) {
      return next(new ApiForbiddenError('Action impossible'));
    }
    for (let i = 0; i < unfollowedUser.features.followers.length; i++) {
      if (unfollowedUser.features.followers[i]._id.equals(req.user._id)) {
        unfollowedUser.features.followers.splice(i, 1);
      }
    }
    for (let i = 0; i < req.user.features.follows.length; i++) {
      if (req.user.features.follows[i]._id.equals(unfollowedUser._id)) {
        req.user.features.follows.splice(i, 1);
      }
    }
    req.user.timestamp = moment();
    unfollowedUser.timestamp = moment();
    try {
      await UserSaveContext.call(unfollowedUser);
      await UserSaveContext.call(req.user);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: UserSerializer(req.user)
    });
  }
}

export default UserFeaturesController;
