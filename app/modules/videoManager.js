import Video from 'models/videos/videoSchema';

class VideoManager {
  static async findById(videoId) {
    return await Video.findById(videoId);
  }

  static async findByIdApiVideo(videoId) {
    return await Video.findOne({ idApiVideo: videoId });
  }

  static async findByOwner(req) {
    return await Video.find({ idOwner: req });
  }

  static async findByVideoAndOwner(owner, apiVideo) {
    return await Video.find({ idOwner: owner, idApiVideo: apiVideo });
  }

  static async pagination(filter, option) {
    return await Video.paginate(filter, option);
  }
}

export default VideoManager;
