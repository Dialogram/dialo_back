import Ticket from 'models/tickets/ticketSchema';

class TicketManager {
  static async findOpenTicket() {
    return await Ticket.find({ status: { $ne: 'close' } });
  }
  static async findByUser(userId) {
    return await Ticket.find({ idOwner: userId });
  }
  static async findById(idDocument) {
    return await Ticket.findById(idDocument);
  }

  static async findByCategory(req) {
    return await Ticket.find({ category: req.params.category });
  }

  static async findByStatus(req) {
    return await Ticket.find({ status: req.params.status });
  }

  /*static async searchByKeyword(req) {
    return (await Ticket.search(req))
  }*/
}

export default TicketManager;
