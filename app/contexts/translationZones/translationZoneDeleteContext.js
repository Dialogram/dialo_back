import { ApiServerError } from 'modules/apiError';

class TranslationZoneDeleteContext {
  static async call(zone, query) {
    await zone.remove(query, err => {
      if (err) {
        throw new ApiServerError(err.message);
      }
    });
  }
}

export default TranslationZoneDeleteContext;
