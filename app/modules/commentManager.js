import Comment from 'models/features/comments/commentSchema';

class CommentManager {
  static async findById(idComment) {
    return await Comment.findById(idComment);
  }

  static async findOneByLike(idComment, ownerId) {
    return await Comment.findOne({ _id: idComment, likes: ownerId });
  }
}

export default CommentManager;
