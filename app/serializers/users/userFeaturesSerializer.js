const userFeaturesSerializer = features => {
  if (!features) {
    return {};
  }

  const data = {
    follows: features.follows,
    followers: features.followers,
    documentsLiked: features.documentsLiked,
    documentsCommented: features.documentsCommented,
    documentsFavorite: features.documentsFavorite
  };
  return data;
};

export default userFeaturesSerializer;
