import fs from 'fs';
import pdf from 'pdf-parse';
import { ApiBadRequestError } from 'modules/apiError';

class DocumentHelper {
  static async getNbPages(filename) {
    let countPages = 0;
    let dataBuffer = fs.readFileSync(global.env.upload.folder_path + filename);

    await pdf(dataBuffer)
      .then(function(data) {
        countPages = data.numpages;
      })
      .catch(err => {
        throw new ApiBadRequestError(err.message);
      });
    return countPages;
  }
}

export default DocumentHelper;
