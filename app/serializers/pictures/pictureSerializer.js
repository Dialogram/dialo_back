const pictureSerializer = picture => {
  if (!picture) {
    return {};
  }

  const data = {
    url: picture.url,
    public_id: picture.public_id
  };
  return data;
};

export default pictureSerializer;
