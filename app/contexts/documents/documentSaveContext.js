import { ApiBadRequestError } from 'modules/apiError';

class DocumentSaveContext {
  static async call(document) {
    try {
      await document.save();
    } catch (err) {
      throw new ApiBadRequestError(err.message);
    }
    return document;
  }
}

export default DocumentSaveContext;
