import { ApiBadRequestError } from 'modules/apiError';
import jwt from 'jsonwebtoken';

class UserTokenContext {
  static async createForgotPasswordToken(user, expirationTime) {
    const token = jwt.sign({ userId: user._id }, global.env.key.jwtSecret, {
      algorithm: 'HS256',
      expiresIn: expirationTime
    });

    user.tokenReset.tokenPassword = token;
    try {
      await user.save();
    } catch (err) {
      throw new ApiBadRequestError(err.message);
    }
    return user;
  }

  static async createConfirmAccountToken(user, expirationTime) {
    const token = jwt.sign({ userId: user._id }, global.env.key.jwtSecret, {
      algorithm: 'HS256',
      expiresIn: expirationTime
    });

    user.settings.confirmedAccount.token = token;
    try {
      await user.save();
    } catch (err) {
      throw new ApiBadRequestError(err.message);
    }
    return user;
  }

  static async createTokenAndUpdateTmpEmail(user, expirationTime, newEmail) {
    const token = jwt.sign({ userId: user._id }, global.env.key.jwtSecret, {
      algorithm: 'HS256',
      expiresIn: expirationTime
    });

    user.tokenReset.resetEmail.tokenEmail = token;
    user.tokenReset.resetEmail.updateEmail = newEmail;
    return user;
  }
}

export default UserTokenContext;
