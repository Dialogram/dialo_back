import TranslationZone from 'models/translationZones/translationZoneSchema';
import { ApiBadRequestError } from 'modules/apiError';
import Schema from 'mongoose';

class TranslationZoneDuplicateContext {
  static async call(req) {
    let zone = new TranslationZone(req);
    zone._id = Schema.Types.ObjectId();
    zone.isNew = true;
    try {
      await zone.save();
    } catch (err) {
      throw new ApiBadRequestError(err.message);
    }
    return zone;
  }
}

export default TranslationZoneDuplicateContext;
