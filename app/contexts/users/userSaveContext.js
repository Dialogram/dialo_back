import { ApiBadRequestError } from 'modules/apiError';

class UserSaveContext {
  static async call(user) {
    try {
      await user.save();
    } catch (err) {
      throw new ApiBadRequestError(err._message);
    }
    return user;
  }
}

export default UserSaveContext;
