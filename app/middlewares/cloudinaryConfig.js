import multer from 'multer';
import cloudinaryStorage from 'multer-storage-cloudinary';
import cloudinary from 'cloudinary';

class CloudinaryConfig {
  static configure() {
    cloudinary.config({
      cloud_name: global.env.cloudinary.name,
      api_key: global.env.cloudinary.key,
      api_secret: global.env.cloudinary.secret
    });
  }

  static configurePicture() {
    CloudinaryConfig.configure();
    const storage = cloudinaryStorage({
      cloudinary: cloudinary,
      folder: global.env.cloudinary.folder,
      allowedFormats: ['jpg', 'png'],
      transformation: [
        { width: 400, height: 400, crop: 'thumb' },
        { dpr: '2.0' }
      ]
    });
    return storage;
  }

  static uploadPicture() {
    return multer({ storage: this.configurePicture() }).single('image');
  }
}

export default CloudinaryConfig;
