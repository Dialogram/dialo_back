import mongoose from 'mongoose';

//tell mongoose to use es6 implementation of promises
//mongoose.Promise = global.Promise;


mongoose.connect('mongodb://localhost/Dialo_API_CI',  {useNewUrlParser: true});
mongoose.connection
    .once('open', () => console.log('Connected!'))
    .on('error', (error) => {
        console.warn('Error : ',error);
    });
    .

    beforeEach((done) => {
        mongoose.connect('mongodb://localhost/Dialo_API_CI', {useNewUrlParser: true}, () => {
            mongoose.connection.db.dropDatabase(() => {
                console.log("DATABASE CLEANED")
                done();
            })
        })
    })