import chai from 'chai';
import chaiHttp from 'chai-http';
import mongoose from 'mongoose';

chai.use(chaiHttp);

let domain = 'http://localhost:8085';
let expect = chai.expect;
let user = {
  nickName: 'UserName',
  profile: {
    firstName: 'John',
    lastName: 'Bryan'
  },
  email: 'my-user@domain.com',
  password: 'password123'
};

let session = {
  email: 'my-user@domain.com',
  password: 'password123'
};

let Invalidsession = {
  email: 'my-user@domin.com',
  password: 'password123'
};

let userToken;

describe('User', () => {
  before(done => {
    mongoose.connect(
      'mongodb://localhost/Dialo_API_CI',
      { useNewUrlParser: true },
      () => {
        mongoose.connection.db.dropDatabase(() => {
          console.log('\x1b[36m%s\x1b[0m', '/!\\ DATABASE CLEANED /!\\');
          done();
        });
      }
    );
  });
  describe('Create/Get User and session', () => {
    it('Create a correct user', done => {
      chai
        .request(domain)
        .post('/api/user')
        .send(user)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          userToken = res.body.includes[0].token;
          done();
        });
    });

    it('Fail to create an existing user', done => {
      chai
        .request(domain)
        .post('/api/user')
        .send(user)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(403);
          done();
        });
    });

    it('Get an existing user', done => {
      chai
        .request(domain)
        .get('/api/user')
        .set('authorization', 'Bearer ' + userToken)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();
        });
    });

    it('Fail to get a user with invalid token', done => {
      chai
        .request(domain)
        .get('/api/user')
        .set('authorization', 'Bearer ' + 'thisisnotvalid')
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(401);
          done();
        });
    });

    it('Create a user session', done => {
      chai
        .request(domain)
        .post('/api/session')
        .send(session)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();
        });
    });

    it('Create an invalid user session', done => {
      chai
        .request(domain)
        .post('/api/session')
        .send(Invalidsession)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(401);
          done();
        });
    });
  });
});
