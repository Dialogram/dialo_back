const apiVideo = require('@api.video/nodejs-sdk');

class Singleton {
  constructor() {
    if (!Singleton.apiVideo) {
      Singleton.apiVideo = new apiVideo.Client({
        username: 'dialogram',
        apiKey: global.env.api_video.key
      });
    }
  }

  getApiVideo() {
    return Singleton.apiVideo;
  }
}

module.exports = Singleton;
