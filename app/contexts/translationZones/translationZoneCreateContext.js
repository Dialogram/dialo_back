import TranslationZone from 'models/translationZones/translationZoneSchema';
import { ApiBadRequestError } from 'modules/apiError';

class TranslationZoneCreateContext {
  static async call(req) {
    const zone = new TranslationZone(req);
    try {
      await zone.save();
    } catch (err) {
      throw new ApiBadRequestError(err.message);
    }
    return zone;
  }
}

export default TranslationZoneCreateContext;
