import multer from 'multer';

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, global.env.upload.folder_path);
  },
  filename: function(req, file, cb) {
    return cb(null, Date.now() + '-' + file.originalname);
  }
});

const PDF_MIMETYPE = 'application/pdf';

const fileFilter = function(req, file, cb) {
  // Check if file is pdf
  if (file.mimetype !== PDF_MIMETYPE) {
    return cb(null, false);
  }
  return cb(null, true);
};

const upload = multer({
  storage,
  fileFilter
});

export default upload;
