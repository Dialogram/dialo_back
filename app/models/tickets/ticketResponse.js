import mongoose, { Schema } from 'mongoose';

const TicketResponseSchema = mongoose.Schema(
  {
    content: {
      type: String,
      required: true,
      minlength: 2
    },
    creationDate: {
      type: Date,
      default: Date.now
    },
    emailOwner: {
      type: String,
      required: true
    },
    idOwner: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    }
  },

  {
    strict: true
  }
);

export default TicketResponseSchema;
