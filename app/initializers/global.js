/*
  Do not import dotenv only
  import dotenv/config and not using dotenv.config()
  because in es6, import is processing before reading file
  https://hacks.mozilla.org/2015/08/es6-in-depth-modules/
*/
import dotenv from 'dotenv';

dotenv.config();
dotenv.config({ path: `.env.${process.env.NODE_ENV}` });
global.env = {
  __APP_VERSION__: process.env.npm_package_version,
  server: {
    addr: process.env.SERVER_ADDR,
    http: process.env.SERVER_HTTP,
    port: process.env.SERVER_PORT,
    url: process.env.SERVER_URL,
    cdn_pdf: process.env.CDN_PDF
  },
  app: {
    url: process.env.APP_URL
  },
  mongo: {
    addr: process.env.MONGO_ADDR,
    db: process.env.MONGO_DB
  },

  key: {
    jwtSecret: process.env.JWT_SECRET,
    accessTokenExpired: process.env.ACCESS_TOKEN_EXPIRED,
    refreshTokenExpired: process.env.REFRESH_TOKEN_EXPIRED
  },

  cloudinary: {
    name: process.env.CLOUDINARY_NAME,
    key: process.env.CLOUDINARY_KEY,
    secret: process.env.CLOUDINARY_SECRET,
    folder: process.env.CLOUDINARY_PROFILE_PICTURES_FOLDER
  },

  api_video: {
    key: process.env.API_VIDEO_KEY
  },

  facebook_auth: {
    id: process.env.FACEBOOK_APP_ID,
    secret: process.env.FACEBOOK_APP_SECRET
  },

  google_auth: {
    id: process.env.GOOGLE_APP_ID,
    secret: process.env.GOOGLE_APP_SECRET
  },

  gmail: {
    gmailAccount: process.env.GMAIL_ACCOUNT,
    gmailSecret: process.env.GMAIL_PWD
  },

  upload: {
    folder_path: process.env.UPLOAD_FOLDER
  },

  request_restrictions: {
    request_limit: process.env.REQUEST_LIMIT
  },

  nodeEnv: process.env.NODE_ENV || 'development',
  __DEV__: process.env.NODE_ENV == 'development'
};
