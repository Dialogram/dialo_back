import TranslationPending from 'models/translationsPending/translationPendingSchema';
import { ApiBadRequestError } from 'modules/apiError';
import Schema from 'mongoose';

class TranslationPendingDuplicateContext {
  static async call(req) {
    let pending = new TranslationPending(req);
    pending._id = Schema.Types.ObjectId();
    pending.isNew = true;
    try {
      await pending.save();
    } catch (err) {
      throw new ApiBadRequestError(err.message);
    }
    return pending;
  }
}

export default TranslationPendingDuplicateContext;
