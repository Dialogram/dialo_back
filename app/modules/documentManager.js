import Document from 'models/documents/documentSchema';

class DocumentManager {
  static async findById(idDocument) {
    return await Document.findById(idDocument);
  }

  static async findByIdAndPopulateComments(idDocument) {
    return await Document.findById(idDocument).populate({
      path: 'features.comments',
      populate: {
        path: 'ownerId',
        select: {
          _id: 1,
          nickName: 1,
          'profile.firstName': 1,
          'profile.lastName': 1,
          'profile.certificated': 1,
          'profile.profilePicture': 1
        }
      }
    });
  }

  static async findByIdAndPopulateTranslationZones(idDocument) {
    return await Document.findById(idDocument).populate({
      path: 'idMasterTranslation',
      populate: {
        path: 'zones'
      }
    });
  }

  static async findOneByFavorite(idDocument, ownerId) {
    return await Document.findOne({
      _id: idDocument,
      'features.favorites': ownerId
    });
  }

  static async findOneByLike(idDocument, ownerId) {
    return await Document.findOne({
      _id: idDocument,
      'features.likes': ownerId
    });
  }

  static async findByCategory(req) {
    return await Document.find({ category: req.params.category });
  }

  static async findByOwner(req) {
    return await Document.find({ idOwner: req });
  }

  static async searchByKeyword(req) {
    return await Document.search(req);
  }

  static async pagination(filter, option) {
    return await Document.paginate(filter, option);
  }
}

export default DocumentManager;
