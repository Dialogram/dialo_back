
//https://www.npmjs.com/package/migrate-mongo
module.exports = {
  up(db) {
    return db.collection('tickets').find({ responses: { $exists: false } }).forEach(result => {
      if (!result) return next('All docs have test')

      result.responses = [];
      return db.collection('tickets').save(result)
    });
  },
  down(db) {
    return db.collection('tickets').update(
      {
        responses: { $exists: true }
      },
      {
        $unset: { responses: [] },
      },
      { multi: true }
    );
  }
};
