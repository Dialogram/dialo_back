import { ApiServerError } from 'modules/apiError';

class VideoDeleteOneContext {
  static async call(video, query) {
    await video.remove(query, err => {
      if (err) {
        throw new ApiServerError(err.message);
      }
    });
  }
}

export default VideoDeleteOneContext;
