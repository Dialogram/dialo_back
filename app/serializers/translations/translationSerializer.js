const translationSerializer = translation => {
  if (!translation) {
    return {};
  }

  const data = {
    type: 'translation',
    id: translation.id,
    idDocument: translation.idDocument,
    idOwner: translation.idOwner,
    zones: translation.zones,
    version: translation.version,
    isMaster: translation.isMaster,
    timestamp: translation.timestamp,
    certificated: translation.certificated,
    idMasterTranslation: translation.idMasterTranslation,
    forkedTranslations: translation.forkedTranslations,
    isForked: translation.isForked,
    pushed: translation.pushed
  };
  return data;
};

export default translationSerializer;
