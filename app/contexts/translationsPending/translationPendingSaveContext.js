import { ApiBadRequestError } from 'modules/apiError';

class TranslationPendingSaveContext {
  static async call(pending) {
    try {
      await pending.save();
    } catch (err) {
      throw new ApiBadRequestError(err.message);
    }
    return pending;
  }
}

export default TranslationPendingSaveContext;
