import Document from 'models/documents/documentSchema';
import { ApiBadRequestError } from 'modules/apiError';

class DocumentCreateContext {
  static async call(req) {
    let document;

    document = new Document(req);
    try {
      await document.save();
    } catch (err) {
      throw new ApiBadRequestError(err.message);
    }
    return document;
  }
}

export default DocumentCreateContext;
