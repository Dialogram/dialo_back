import TranslationVideoSerializer from 'serializers/translationVideos/translationVideoSerializer';
import TranslationVideoCreateContext from 'contexts/translationVideos/translationVideoCreateContext';
import VideoSaveContext from 'contexts/videos/videoSaveContext';
import VideoDeleteOneContext from 'contexts/videos/videoDeleteOneContext';
import PaginationHelper from 'helpers/pagination/paginationHelper';
import TranslationVideoManager from 'modules/translationVideoManager';
import PaginationSerializer from 'serializers/paginations/paginationSerializer';
import Singleton from 'controllers/singleton/singleton';
import moment from 'moment';

import {
  ApiNotFoundError,
  ApiForbiddenError,
  ApiServerError,
  ApiBadRequestError
} from 'modules/apiError';

let client = new Singleton().getApiVideo();

class TranslationVideoController {
  static async create(req, res, next) {
    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (!req.body.title || !req.body.public || !req.file) {
      return next(
        new ApiBadRequestError('Tous les champs doivent être renseignés')
      );
    }
    const result = client.videos.upload(req.file.path, {
      title: req.body.title,
      description: req.body.description
    });
    let createdVideo;
    result
      .then(async video => {
        try {
          createdVideo = await TranslationVideoCreateContext.call(req, video);
        } catch (err) {
          return next(err);
        }
        return res.json({
          data: TranslationVideoSerializer(createdVideo)
        });
      })
      .catch(err => {
        return next(new ApiServerError(err.message));
      });
  }

  static searchVideo(req, res) {
    let result = client.videos.search({ currentPage: 1, pageSize: 25 });

    result.then(function(videos) {
      let video;
      for (let x = 0; x < videos.length; x += 1) {
        if (Object.prototype.hasOwnProperty.call(videos, x)) {
          video = videos[x];
        }
      }
    });
    return res.json({
      data: {}
    });
  }

  static async getById(req, res, next) {
    let video;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (
      !(video = await TranslationVideoManager.findByIdApiVideo(
        req.params.idApiVideo
      ))
    ) {
      return next(new ApiNotFoundError('Ressource introuvable'));
    }
    return res.json({
      data: TranslationVideoSerializer(video)
    });
  }

  static async getMyTranslationVideos(req, res, next) {
    let video;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    const pagination = await PaginationHelper.setDefaultPagination(req.query);
    let options = await PaginationHelper.parseOption(pagination, req);
    const filter = await PaginationHelper.cleanFilter(req.query);

    try {
      video = await TranslationVideoManager.pagination(filter, options);
    } catch (err) {
      return next(err);
    }

    return res.json({
      data: video.docs.map(item => TranslationVideoSerializer(item)),
      includes: [PaginationSerializer(video)]
    });
  }

  static async updateData(req, res, next) {
    let video;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (
      !(video = await TranslationVideoManager.findByIdApiVideo(
        req.params.idApiVideo
      ))
    ) {
      return next(new ApiNotFoundError('Ressource introuvable'));
    }
    if (!req.body.title || !req.body.description || !req.body.public) {
      return next(
        new ApiBadRequestError('Tous les champs doivent être renseignés')
      );
    }
    video.title = req.body.title;
    video.description = req.body.description;
    video.timestamp = moment();
    client.videos.update(req.params.idApiVideo, {
      title: req.body.title,
      description: req.body.description
    });
    video.public = req.body.public;
    video.category = req.body.category;
    try {
      await VideoSaveContext.call(video);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: TranslationVideoSerializer(video)
    });
  }

  static async delete(req, res, next) {
    let video;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (
      !(video = await TranslationVideoManager.findByIdApiVideo(
        req.params.idApiVideo
      ))
    ) {
      return next(new ApiNotFoundError('Ressource introuvable'));
    }
    let result = client.videos.delete(req.params.idApiVideo);
    result
      .then(async statusCode => {
        try {
          await VideoDeleteOneContext.call(video, { _id: video.id });
        } catch (err) {
          return next(err);
        }
        return res.json({
          data: {},
          includes: []
        });
      })
      .catch(err => {
        return next(new ApiServerError(err.message));
      });
  }

  static async deleteAllTranslationVideos(req, res, next) {
    let videos;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (!(videos = await TranslationVideoManager.findByOwner(req.user._id))) {
      next();
    }
    for (let i = 0; i < videos.length; i += 1) {
      try {
        await client.videos.delete(videos[i].idApiVideo);
        await VideoDeleteOneContext.call(videos[i], { _id: videos[i].id });
      } catch (err) {
        return next(err);
      }
    }
    next();
  }
}

export default TranslationVideoController;
