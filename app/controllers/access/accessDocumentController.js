import DocumentManager from 'modules/documentManager';
import AccessRightManager from 'modules/accessRightManager';
import DocumentSerializer from 'serializers/documents/documentSerializer';
import DocumentSaveContext from 'contexts/documents/documentSaveContext';
import VideoSerializer from 'serializers/videos/videoSerializer';
import VideoSaveContext from 'contexts/videos/videoSaveContext';
import VideoManager from 'modules/videoManager';

import {
  ApiNotFoundError,
  ApiUnauthorizedError,
  ApiForbiddenError,
  ApiBadRequestError,
  ApiServerError
} from 'modules/apiError';

class AccessDocumentController {
  static async editAccess(req, res, next) {
    let entity, type;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }

    if (req.params.idDocument) {
      type = 'doc';
    } else if (req.params.idApiVideo) {
      type = 'video';
    } else {
      return next(new ApiForbiddenError("Type d'entité non reconnu"));
    }

    // Type document.
    if (type == 'doc') {
      try {
        entity = await DocumentManager.findById(req.params.idDocument);
      } catch (err) {
        return next(new ApiNotFoundError('Ressource introuvable'));
      }
    }
    // Type video.
    else {
      if (
        !(entity = await VideoManager.findByIdApiVideo(req.params.idApiVideo))
      ) {
        return next(new ApiNotFoundError('Ressource introuvable'));
      }
    }

    if (
      JSON.stringify(entity.access.idOwner) != JSON.stringify(req.user._id) &&
      !entity.access.moderator.indexOf(req.user._id) < 0
    ) {
      return next(new ApiForbiddenError('Permissions non suffisantes'));
    }

    if (req.params.role == 'moderator') {
      if (entity.access.moderator.includes(req.params.idUser)) {
        return next(new ApiForbiddenError("L'utilisateur est déjà modérateur"));
      }
      entity.access.moderator.push(req.params.idUser);
      if (entity.access.collaborator.includes(req.params.idUser)) {
        try {
          entity.access.collaborator.splice(
            entity.access.collaborator.indexOf(req.params.idUser, 1)
          );
        } catch (err) {
          return next(
            new ApiServerError('Impossible de suppprimer le précédent rôle')
          );
        }
      }
    } else if (req.params.role == 'collaborator') {
      if (entity.access.collaborator.includes(req.params.idUser)) {
        return next(
          new ApiForbiddenError("L'utilisateur est déjà collaborateur")
        );
      }
      entity.access.collaborator.push(req.params.idUser);

      if (entity.access.moderator.includes(req.params.idUser)) {
        try {
          entity.access.moderator.splice(
            entity.access.moderator.indexOf(req.params.idUser, 1)
          );
        } catch (err) {
          return next(
            new ApiServerError('Impossible de suppprimer le précédent rôle')
          );
        }
      }
    } else {
      return next(new ApiForbiddenError("Le rôle spécifié n'existe pas"));
    }

    try {
      await AccessRightManager.PopulateAccess(entity, req.user._id);
    } catch (err) {
      return next(
        new ApiBadRequestError(
          'Erreur pendant la récupération des accès utilisateur'
        )
      );
    }

    // Document access.
    if (type == 'doc') {
      try {
        await DocumentSaveContext.call(entity);
      } catch (err) {
        return next("La mise à jour du rôle de l'utilisateur à échoué");
      }

      return res.json({
        data: DocumentSerializer(entity)
      });
    }

    // Video access.
    else {
      try {
        await VideoSaveContext.call(entity);
      } catch (err) {
        return next("La mise à jour du rôle de l'utilisateur à échoué");
      }

      return res.json({
        data: VideoSerializer(entity)
      });
    }
  }

  // Revoke access to an user.
  static async removeAccess(req, res, next) {
    let entity, type;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }

    if (req.params.idDocument) {
      type = 'doc';
    } else if (req.params.idApiVideo) {
      type = 'video';
    } else {
      return next(new ApiForbiddenError("Type d'entité non reconnu"));
    }

    // Type document.
    if (type == 'doc') {
      try {
        entity = await DocumentManager.findById(req.params.idDocument);
      } catch (err) {
        return next(new ApiNotFoundError('Ressource introuvable'));
      }
    }
    // Type video.
    else {
      if (
        !(entity = await VideoManager.findByIdApiVideo(req.params.idApiVideo))
      ) {
        return next(new ApiNotFoundError('Ressource introuvable'));
      }
    }

    try {
      await AccessRightManager.PopulateAccess(entity, req.user._id);
    } catch (err) {
      return next(
        new ApiBadRequestError(
          'Erreur pendant la récupération des accès utilisateur'
        )
      );
    }

    if (
      JSON.stringify(entity.access.idOwner) != JSON.stringify(req.user._id) &&
      entity.access.moderator.indexOf(req.user._id) < 0
    ) {
      return next(new ApiForbiddenError('Permissions non suffisantes'));
    }

    if (req.params.role === 'moderator') {
      if (entity.access.moderator.includes(req.params.idUser)) {
        try {
          entity.access.moderator.splice(
            entity.access.moderator.indexOf(req.params.idUser, 1)
          );
        } catch (err) {
          return next(
            new ApiServerError('Impossible de suppprimer le précédent rôle')
          );
        }
      } else {
        return next(
          new ApiUnauthorizedError("L'utilisateur n'est pas modérateur")
        );
      }
    }

    else if (req.params.role == 'collaborator') {
      console.log('ta grosse mere');
      if (entity.access.collaborator.includes(req.params.idUser)) {
        try {
          entity.access.collaborator.splice(
            entity.access.collaborator.indexOf(req.params.idUser, 1)
          );
        } catch (err) {
          return next(
            new ApiServerError('Impossible de suppprimer le précédent rôle')
          );
        }
      } else {
        return next(
          new ApiForbiddenError("L'utilisateur n'est pas collaborateur")
        );
      }
    }

    else if (req.params.role == 'owner') {
      if (entity.idOwner == req.params.idUser) {
        return next(
          new ApiForbiddenError(
            'Le propriétaire du document ne peut être déchu'
          )
        );
      }
    } else {
      return next(new ApiForbiddenError("Le rôle spécifé n'existe pas"));
    }

    // Document access.
    if (type == 'doc') {
      try {
        await DocumentSaveContext.call(entity);
      } catch (err) {
        return next("La mise à jour du rôle de l'utilisateur à échoué");
      }

      return res.json({
        data: DocumentSerializer(entity)
      });
    }

    // Video access.
    else {
      try {
        await VideoSaveContext.call(entity);
      } catch (err) {
        return next("La mise à jour du rôle de l'utilisateur à échoué");
      }

      return res.json({
        data: VideoSerializer(entity)
      });
    }
  }
}

export default AccessDocumentController;
