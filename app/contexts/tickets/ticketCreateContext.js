import Ticket from 'models/tickets/ticketSchema';
import { ApiBadRequestError } from 'modules/apiError';

class TicketCreateContext {
  static async call(req) {
    let ticket;

    ticket = new Ticket(req);
    try {
      await ticket.save();
    } catch (err) {
      throw new ApiBadRequestError(err.message);
    }
    return ticket;
  }
}

export default TicketCreateContext;
