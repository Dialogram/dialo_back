import { ApiServerError } from 'modules/apiError';

class CommentDeleteContext {
  static async call(comment, querry) {
    await comment.remove(querry, err => {
      if (err) {
        throw new ApiServerError(err.message);
      }
    });
  }
}

export default CommentDeleteContext;
