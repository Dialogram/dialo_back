import mongoose from 'mongoose';
import PictureSchema from 'models/pictures/pictureSchema';

const UserProfileSchema = mongoose.Schema(
  {
    registerDate: {
      type: Date,
      default: Date.now
    },
    createdWith: {
      provider: {
        type: String,
        enum: ['Dialogram', 'Facebook', 'Google'],
        default: 'Dialogram'
      },
      userId: {
        type: String,
        default: null
      }
    },
    firstName: {
      type: String,
      minlength: 3,
      maxlength: 22,
      required: true,
      trim: true
    },
    lastName: {
      type: String,
      minlength: 3,
      maxlength: 22,
      required: true,
      trim: true
    },
    profilePicture: {
      type: PictureSchema,
      default: {}
    },
    birthday: {
      type: String,
      default: null
    },
    gender: {
      type: String,
      enum: ['male', 'female', 'other', null],
      default: null
    },
    country: {
      type: String,
      default: null
    },
    hometown: {
      type: String,
      default: null
    },
    description: {
      type: String,
      maxlength: 250,
      default: null
    },
    certificated: {
      type: Boolean,
      default: false
    }
  },
  {
    strict: true,
    _id: false
  }
);

export default UserProfileSchema;
