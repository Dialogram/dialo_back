import TranslationSerializer from 'serializers/translations/translationSerializer';
import TranslationPendingSerializer from 'serializers/translationsPending/translationPendingSerializer';
import TranslationCreateContext from 'contexts/translations/translationCreateContext';
import TranslationPendingCreateContext from 'contexts/translationsPending/translationPendingCreateContext';
import TranslationSaveContext from 'contexts/translations/translationSaveContext';
import TranslationDeleteContext from 'contexts/translations/translationDeleteContext';
import TranslationManager from 'modules/translationManager';
import TranslationHelper from 'helpers/translation/translationHelper';
import TranslationZoneController from 'controllers/translationZones/translationZoneController';
import DocumentManager from 'modules/documentManager';
import DocumentSaveContext from 'contexts/documents/documentSaveContext';

import moment from 'moment';

import {
  ApiNotFoundError,
  ApiBadRequestError,
  ApiForbiddenError
} from 'modules/apiError';

class TranslationController {
  static async createMaster(user, document, next) {
    let translation;

    let givenParams = {
      idOwner: user._id,
      idDocument: document._id,
      isMaster: true
    };
    try {
      translation = await TranslationCreateContext.call(givenParams);
      document.idMasterTranslation = translation._id;
      await DocumentSaveContext.call(document);
    } catch (err) {
      return next(err);
    }
    return translation;
  }

  static async createNew(req, res, next) {
    let document;
    let translation;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }

    if (!(document = await DocumentManager.findById(req.params.idDocument))) {
      return next(new ApiNotFoundError('Document introuvable'));
    }
    let givenParams = {
      idOwner: req.user._id,
      idDocument: document._id,
      description: req.body.description,
      idMasterTranslation: document.idMasterTranslation
    };
    try {
      translation = await TranslationCreateContext.call(givenParams);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: TranslationSerializer(translation)
    });
  }
  static async getMaster(req, res, next) {
    let masterTranslation;
    let page = 0;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (!(await DocumentManager.findById(req.params.idDocument))) {
      return next(new ApiNotFoundError('Document introuvable'));
    }
    if (
      !(masterTranslation = await TranslationManager.findOneMasterByIdDocument(
        req.params.idDocument
      ))
    ) {
      return next(new ApiNotFoundError('Traduction master introuvable'));
    }
    if (req.query.page && Number.isInteger(req.query.page)) {
      if (
        !(masterTranslation = await TranslationManager.findByIdAndPopulateZonesPage(
          masterTranslation.id,
          page
        ))
      ) {
        return next(new ApiNotFoundError('Traduction introuvable'));
      }
    } else {
      if (
        !(masterTranslation = await TranslationManager.findByIdAndPopulateZones(
          masterTranslation.id
        ))
      ) {
        return next(new ApiNotFoundError('Traduction introuvable'));
      }
    }

    return res.json({
      data: TranslationSerializer(masterTranslation)
    });
  }

  static async getById(req, res, next) {
    let translation;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (req.query.page && Number.isInteger(req.query.page)) {
      if (
        !(translation = await TranslationManager.findByIdAndPopulateZonesPage(
          req.params.idTranslation,
          req.query.page
        ))
      ) {
        return next(new ApiNotFoundError('Traduction introuvable'));
      }
    } else {
      if (
        !(translation = await TranslationManager.findByIdAndPopulateZones(
          req.params.idTranslation
        ))
      ) {
        return next(new ApiNotFoundError('Traduction introuvable'));
      }
    }
    return res.json({
      data: TranslationSerializer(translation)
    });
  }

  static async fork(req, res, next) {
    let translation;
    let forkedTranslation;
    let document;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (
      !(translation = await TranslationManager.findById(
        req.params.idTranslation
      ))
    ) {
      return next(new ApiNotFoundError('Traduction introuvable'));
    }
    if (!(document = await DocumentManager.findById(translation.idDocument))) {
      return next(new ApiNotFoundError('Document introuvable'));
    }
    forkedTranslation = {
      idOwner: req.user._id,
      idDocument: req.params.idDocument,
      isForked: true,
      zones: [],
      idMasterTranslation: document.idMasterTranslation
    };

    try {
      if (translation.zones.length > 0) {
        forkedTranslation.zones = await TranslationZoneController.forkZones(
          translation.zones
        );
      }
      forkedTranslation = await TranslationCreateContext.call(
        forkedTranslation
      );
      translation.forkedTranslations.push(forkedTranslation._id);
      await TranslationSaveContext.call(translation);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: TranslationSerializer(forkedTranslation)
    });
  }

  static async pullMaster(req, res, next) {
    let masterTranslation;
    let translation;
    let pendingTranslation;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (
      !(translation = await TranslationManager.findByIdAndPopulateZones(
        req.params.idTranslation
      ))
    ) {
      return next(new ApiNotFoundError('Traduction introuvable'));
    }
    if (translation.isMaster == true) {
      return next(
        new ApiBadRequestError('La traduction master ne peut être pull')
      );
    }
    if (
      !(masterTranslation = await TranslationManager.findOneMasterByIdTranslationAndPopulateZone(
        req.params.idTranslation
      ))
    ) {
      return next(new ApiNotFoundError('Traduction master introuvable'));
    }

    pendingTranslation = await TranslationHelper.pullMasterTranslation(
      masterTranslation,
      translation
    );
    try {
      pendingTranslation = await TranslationPendingCreateContext.call(
        pendingTranslation
      );
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: TranslationPendingSerializer(pendingTranslation)
    });
  }

  static async diff(req, res, next) {
    let translation;
    let pendingTranslation;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }

    if (
      !(translation = await TranslationManager.findByIdAndPopulateZones(
        req.params.idTranslation
      ))
    ) {
      return next(new ApiNotFoundError('Traduction introuvable'));
    }
    if (!(await TranslationManager.findById(translation.idMasterTranslation))) {
      return next(new ApiNotFoundError('Traduction master introuvable'));
    }
    pendingTranslation = await TranslationHelper.compareMasterTranslation(
      translation
    );
    return res.json({
      data: TranslationPendingSerializer(pendingTranslation)
    });
  }

  static async getAllByDocumentId(req, res, next) {
    let translation;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (!(await DocumentManager.findById(req.params.idDocument))) {
      return next(new ApiNotFoundError('Document introuvable'));
    }
    if (
      !(translation = await TranslationManager.findByIdDocument(
        req.params.idDocument
      ))
    ) {
      return next(new ApiNotFoundError('Translatione introuvable'));
    }
    return res.json({
      data: translation.map(item => TranslationSerializer(item))
    });
  }

  static async mergeToMaster(req, res, next) {
    let translation;
    let master;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (!('delete' in req.body)) {
      return next(
        new ApiBadRequestError(
          'Information manquante dans le corps de la requête'
        )
      );
    }
    if (!(await DocumentManager.findById(req.params.idDocument))) {
      return next(new ApiNotFoundError('Document introuvable'));
    }
    if (
      !(translation = await TranslationManager.findById(
        req.params.idTranslation
      ))
    ) {
      return next(new ApiNotFoundError('Translatione introuvable'));
    }
    if (
      !(master = await TranslationManager.findOneMasterByIdDocument(
        req.params.idDocument
      ))
    ) {
      return next(new ApiNotFoundError('Translatione master introuvable'));
    }
    if (translation.merged == true) {
      return next(new ApiForbiddenError('Translatione déjà merge'));
    }
    master.translation = [];
    for (let i = 0; i < translation.translation.length; i++) {
      master.translation.push(translation.translation[i]);
    }
    master.version = master.version + 0.1;
    master.timestamp = moment();
    try {
      if (req.body.delete == false) {
        translation.merged = true;
        translation.timestamp = moment();
        await TranslationSaveContext.call(translation);
      } else {
        await TranslationDeleteContext.call(translation, {
          _id: translation._id
        });
      }
      await TranslationSaveContext.call(master);
    } catch (err) {
      return next(err);
    }
    if (
      !(master = await TranslationManager.findByIdAndPopulateVideos(master.id))
    ) {
      return next(new ApiNotFoundError('Traduction introuvable'));
    }
    return res.json({
      data: TranslationSerializer(master)
    });
  }

  static async delete(req, res, next) {
    let translation;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (!(await DocumentManager.findById(req.params.idDocument))) {
      return next(new ApiNotFoundError('Document introuvable'));
    }
    if (
      !(translation = await TranslationManager.findById(
        req.params.idTranslation
      ))
    ) {
      return next(new ApiNotFoundError('Translatione introuvable'));
    }
    try {
      await TranslationDeleteContext.call(translation, {
        _id: translation._id
      });
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: {}
    });
  }
}

export default TranslationController;
