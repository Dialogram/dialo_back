import { ApiBadRequestError } from 'modules/apiError';

class TranslationSaveContext {
  static async call(translation) {
    try {
      await translation.save();
    } catch (err) {
      throw new ApiBadRequestError(err.message);
    }
    return translation;
  }
}

export default TranslationSaveContext;
