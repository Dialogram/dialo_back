import { ApiBadRequestError } from 'modules/apiError';

class VideoSaveContext {
  static async call(video) {
    try {
      await video.save();
    } catch (err) {
      throw new ApiBadRequestError(err.message);
    }
    return video;
  }
}

export default VideoSaveContext;
