import { ApiServerError } from 'modules/apiError';

class TranslationPendingDeleteContext {
  static async call(pending, query) {
    await pending.remove(query, err => {
      if (err) {
        throw new ApiServerError(err.message);
      }
    });
  }
}

export default TranslationPendingDeleteContext;
