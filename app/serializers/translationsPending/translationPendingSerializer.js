const translationPendingSerializer = translationPending => {
  if (!translationPending) {
    return {};
  }

  const data = {
    type: 'translationPending',
    id: translationPending.id,
    idOwner: translationPending.idOwner,
    idTranslation: translationPending.idTranslation,
    version: translationPending.version,
    new: translationPending.new,
    update: translationPending.update,
    delete: translationPending.delete
  };
  return data;
};

export default translationPendingSerializer;
