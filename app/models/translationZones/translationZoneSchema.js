import mongoose from 'mongoose';
import Schema from 'mongoose';

const TranslationZoneSchema = mongoose.Schema(
  {
    x: {
      type: Number,
      required: true
    },
    y: {
      type: Number,
      required: true
    },
    width: {
      type: Number,
      required: true
    },
    height: {
      type: Number,
      required: true
    },
    idVideo: {
      type: Schema.Types.ObjectId,
      ref: 'TranslationVideo'
    },
    page: {
      type: Number,
      required: true
    },
    idMaster: {
      type: Schema.Types.ObjectId,
      ref: 'TranslationZone',
      default: null
    },
    state: {
      type: String,
      required: true,
      enum: ['MASTER', 'MASTER_UPDATE', 'NEW', 'DELETED']
    }
  },
  {
    strict: true
  }
);

const Zone = mongoose.model('Zone', TranslationZoneSchema);

export default Zone;
