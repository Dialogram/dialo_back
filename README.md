# Dialogram Back-End API

Dialogram is a collaborative platform, available on mobile and web, which aims to provide interpretations in French Sign Language (LSF) of media and documents.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* [NodeJS](https://nodejs.org/en/download/) - JavaScript runtime built on Chrome's V8 JavaScript engine.
* [MongoDB](https://www.mongodb.com/download-center/community) - Cross-platform document-oriented database program.

#### APIs

* [Cloudinary](https://cloudinary.com/) - provides a cloud-based image and video management solution. 
* [Video API](https://api.video/) - cloud infrastructure allows you to host and broadcast your videos worldwide.
* [Facebook API](https://developers.facebook.com/apps/) - authentication APIs from Facebook that developers can use to help their users connect and share for their website or application.

[NodeMailer](https://nodemailer.com/about/) is used for sending email to users, you should create a Gmail account and [allow access](https://myaccount.google.com/lesssecureapps/) for non secure apps to access Gmail.

### Installing

For windows users, you should install Win-Node-Env

```
npm install -g win-node-env
```

After installing the prerequisites and cloning the project, you should install yarn.

```
npm install -g yarn
```

At the root of the project install all the dependencies

```
yarn install
```

and create a ".env" file who looks like this

```
/*      server config   */
SERVER_HTTP=http        // http or https
SERVER_PORT="port_number"
SERVER_ADDR="host"      // examples: api.dialogram.fr - localhost
SERVER_URL="url_server" // examples: api.dialogram.fr - localhost:3000

APP_URL="app_url"       // examples: app.dialogram.fr - localhost:8080

/*      mongodb         */
MONGO_ADDR="localhost"
MONGO_DB="dialogram_database"

REQUEST_LIMIT=500
JWT_SECRET="[JsonWebToken_Secret]"

/*      nodemailer.js   */
GMAIL_ACCOUNT="your_account@gmail.com"
GMAIL_PWD="your_password"

/*      cloudinary      */
CLOUDINARY_NAME="your_cloudinary_name"
CLOUDINARY_KEY="your_cloudinary_key"
CLOUDINARY_SECRET="your_cloudinary_secret"
CLOUDINARY_PROFILE_PICTURES_FOLDER="your_cloudinary_folder"

/*      Store document  */
UPLOAD_FOLDER="/home/user/upload_folder/" // Don't forget the last '/' !

/*      Api Video       */
API_VIDEO_KEY="your_api_key"

/*      Facebook API    */
FACEBOOK_APP_ID="your_facebook_app_id"
FACEBOOK_APP_SECRET="your_facebook_app_secret"

/*      Google API    */
GOOGLE_APP_ID="your_google_app_id"
GOOGLE_APP_SECRET="your_google_app_secret"
```

## Running the app

```
yarn start
```

## Documentation

Please read the [documentation](https://dialogram.github.io/dialo_back_doc/) of the project.

## Running the tests

All tests available in the folder 'test'.

```
yarn test
```

## Deployment

Not available.

## Built With

* [Express](https://expressjs.com/fr/) - NodeJS framework

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

Not available.

## Authors

* **Dialogram Team** - [Members](https://gitlab.com/groups/Dialogram/-/group_members)

See also the list of [contributors](https://gitlab.com/Dialogram/dialo_back/graphs/master) who participated in this project.

## Main Back-End Developpers

* [David Munoz](https://gitlab.com/davidmunoz-dev)
* [Lucas Onillon](https://gitlab.com/lezim)

## CGU

[General Condition of Use](https://dialogram.fr/cgu.html)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
