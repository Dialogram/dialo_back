import TranslationPending from 'models/translationsPending/translationPendingSchema';
import { ApiBadRequestError } from 'modules/apiError';

class TranslationPendingCreateContext {
  static async call(req) {
    const pending = new TranslationPending(req);
    try {
      await pending.save();
    } catch (err) {
      throw new ApiBadRequestError(err.message);
    }
    return pending;
  }
}

export default TranslationPendingCreateContext;
