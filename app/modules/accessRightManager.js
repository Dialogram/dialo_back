import Document from 'models/documents/documentSchema';
import DocumentSaveContext from 'contexts/documents/documentSaveContext';
import VideoSaveContext from 'contexts/videos/videoSaveContext';

import { ApiNotFoundError, ApiBadRequestError } from 'modules/apiError';

class AccessRightManager {
  static async PopulateAccess(entity, idUser) {
    try {
      entity.access.role = await AccessRightManager.GetRole(entity, idUser);
    } catch (err) {
      return new ApiBadRequestError(err);
    }

    try {
      entity.access.moderators = await AccessRightManager.GetModerators(
        entity,
        idUser
      );
    } catch (err) {
      return new ApiBadRequestError(err);
    }

    try {
      entity.access.collaborators = await AccessRightManager.GetCollaborators(
        entity
      );
    } catch (err) {
      return new ApiBadRequestError(err);
    }

    if (entity.source) {
      try {
        await VideoSaveContext.call(entity);
      } catch (err) {
        return "La mise à jour du rôle de l'utilisateur à échoué";
      }
    } else {
      try {
        await DocumentSaveContext.call(entity);
      } catch (err) {
        return "La mise à jour du rôle de l'utilisateur à échoué";
      }
    }
  }

  /*
   ** ACCESS ROLE.
   */
  static async GetCollaborators(entity) {
    let doc;

    try {
      doc = await Document.findById(entity.id);
    } catch (err) {
      return new ApiNotFoundError("Impossible de retrouver l'entité spécifiée");
    }

    return doc.access.collaborator;
  }

  static async GetModerators(entity) {
    let doc;

    try {
      doc = await Document.findById(entity.id);
    } catch (err) {
      return new ApiNotFoundError("Impossible de retrouver l'entité spécifiée");
    }

    return doc.access.moderator;
  }

  static async GetRole(entity, idUser) {
    if (JSON.stringify(entity.idOwner) == JSON.stringify(idUser)) {
      return 'owner';
    } else if (entity.access.moderator.indexOf(idUser) > -1) {
      return 'moderator';
    } else if (entity.access.collaborator.indexOf(idUser) > -1) {
      return 'collaborator';
    } else {
      return 'NONE';
    }
  }
}

export default AccessRightManager;
