import MobileDetect from 'mobile-detect';
import CloudinaryConfig from 'middlewares/cloudinaryConfig';
import ToolsHelper from 'helpers/tools/toolsHelper';
import cloudinary from 'cloudinary';
import uniqid from 'uniqid';
import UserManager from 'modules/userManager';
import NoReplyMailHelper from 'helpers/users/noreplyMailHelper';
import UserTokenContext from 'contexts/users/userTokenContext';
import moment from 'moment';

import { ApiServerError, ApiForbiddenError } from 'modules/apiError';

class UserHelper {
  static getLastSession(user) {
    if (!user || !(user.sessions instanceof Array)) {
      return {};
    }
    return user.sessions.slice(-1)[0] || {};
  }

  static getDeviceName(req) {
    let userAgent = new MobileDetect(req.headers['user-agent']);
    let device = {
      userAgent: userAgent.userAgent(),
      OS: userAgent.os()
    };
    return device;
  }

  static getAvatarURL(firstName, lastName) {
    let url;
    const blue = { background: '0D8ABC', color: 'fff' };
    const red = { background: 'AA0015', color: 'fff' };
    const green = { background: '00AA40', color: 'fff' };
    const yellow = { background: 'D6E001', color: '222' };
    const black = { background: '010101', color: 'fff' };
    const white = { background: 'FFFFFE', color: '222' };

    const colors = [blue, red, green, yellow, black, white];
    const used = colors[ToolsHelper.randomIntInc(0, 5)];
    url =
      'https://ui-avatars.com/api/?name=' +
      firstName +
      '+' +
      lastName +
      '&background=' +
      used.background +
      '&color=' +
      used.color;
    return url;
  }

  static createNickname(firstName, lastName) {
    let nickName;

    if (!firstName || !lastName) {
      nickName = 'id_' + uniqid.process();
      return nickName;
    }
    nickName =
      firstName.charAt(0) + lastName.charAt(0) + '_' + uniqid.process();
    return nickName;
  }

  static async updateNickname(req) {
    let user;

    if (req.body.nickName.length < 5 || req.body.nickName.length > 15) {
      throw new ApiForbiddenError('Taille du pseudonyme invalide ');
    }
    if ((user = await UserManager.findByNickName(req.body.nickName)) != null) {
      throw new ApiForbiddenError('Pseudonyme déjà utilisé');
    }
    req.user.nickName = req.body.nickName;
    req.user.timestamp = moment();
    return req.user;
  }

  static async updateEmail(req) {
    let user;

    if ((user = await UserManager.findByEmail(req.body.email)) != null) {
      throw new ApiForbiddenError(
        'Un compte avec cette adresse courriel existe déjà'
      );
    }
    req.user = await UserTokenContext.createTokenAndUpdateTmpEmail(
      req.user,
      '1d',
      req.body.email
    );
    NoReplyMailHelper.mailInformEmailUpdate(req.user, req.body.email);
    NoReplyMailHelper.mailConfirmEmailUpdate(req.user, req.body.email);
    return req.user;
  }

  static async uploadPictureFromURL(url) {
    let picture;

    CloudinaryConfig.configure();
    if (url != null) {
      await cloudinary.v2.uploader
        .upload(url, {
          type: 'upload',
          folder: global.env.cloudinary.folder,
          eager: [{ width: 400, height: 400, crop: 'thumb' }, { dpr: 2 }]
        })
        .then(result => {
          picture = result;
        })
        .catch(err => {
          throw new ApiServerError(err.message);
        });
    }
    return picture;
  }

  static async deleteCloudinaryPicture(public_id) {
    if (public_id != null) {
      await cloudinary.v2.uploader.destroy(public_id).catch(err => {
        throw new ApiServerError(err.message);
      });
    }
    return;
  }
}

export default UserHelper;
