import Translation from 'models/translations/translationSchema';
import { ApiBadRequestError } from 'modules/apiError';

class TranslationCreateContext {
  static async call(req) {
    const translation = new Translation(req);
    try {
      await translation.save();
    } catch (err) {
      throw new ApiBadRequestError(err.message);
    }
    return translation;
  }
}

export default TranslationCreateContext;
