const commentSerializer = comment => {
  if (!comment) {
    return {};
  }

  const data = {
    type: 'comment',

    id: comment._id,
    comment: comment.comment,
    likes: comment.likes,
    creationDate: comment.creationDate,
    lastUpdateDate: comment.lastUpdateDate,
    edited: comment.edited,
    ownerId: comment.ownerId
  };
  return data;
};

export default commentSerializer;
