import express from 'express';
import ApiAuthentication from 'middlewares/apiAuthentication';
import UserController from 'controllers/users/userController';
import UserFeaturesController from 'controllers/users/userFeaturesController';
import SessionController from 'controllers/sessions/sessionController';
import ForgotPasswordController from 'controllers/users/forgotPasswordController';
import DocumentController from 'controllers/documents/documentController';
import DocumentFeaturesController from 'controllers/documents/documentFeaturesController';
import SettingsController from 'controllers/users/settingsController';
import TranslationController from 'controllers/translations/translationController';
import TranslationZoneController from 'controllers/translationZones/translationZoneController';
import TranslationVideoController from 'controllers/translationVideos/translationVideoController';
import VideoController from 'controllers/videos/videoController';
import VideoApiAuthentication from 'middlewares/videoApiAuthentication';
import passport from 'passport';
import TicketController from 'controllers/tickets/ticketController';
import CloudinaryConfig from 'middlewares/cloudinaryConfig';
import multer from 'middlewares/multer';
import AccessDocumentController from 'controllers/access/accessDocumentController';

class Routes {
  constructor() {
    this.router = express.Router();
    this._setRoutes();
  }

  getRouter() {
    return this.router;
  }

  _setRoutes() {
    this.router
      .route('/password/reset')
      .post(ForgotPasswordController.resetForgotPassword);

    this.router
      .route('/password/reset/:token')
      .post(ForgotPasswordController.updateForgotPasswordByToken);

    this.router.route('/session').post(SessionController.create);

    this.router
      .route('/account/refresh-token/:token')
      .get(UserController.refreshToken);

    this.router
      .route('/auth/facebook')
      .post(passport.authenticate('facebook', { session: false }));

    this.router
      .route('/auth/facebook/callback')
      .post(
        passport.authenticate('facebook', { session: false }),
        UserController.createFromFacebook
      );

    this.router
      .route('/auth/google')
      .post(passport.authenticate('google', { session: false }));

    this.router
      .route('/auth/google/callback')
      .post(
        passport.authenticate('google', { session: false }),
        UserController.createFromGoogle
      );

    this.router
      .route('/user')
      .post(UserController.create)
      .get(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        UserController.me
      );

    this.router
      .route('/user/profile/:idUser')
      .get(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        UserController.getUser
      );

    this.router
      .route('/user/send/verification')
      .put(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveNotConfirmedUser,
        SettingsController.sendMailConfirmAccount
      );

    this.router
      .route('/user/confirm/:token')
      .get(SettingsController.confirmAccount);

    this.router
      .route('/search/user/:nickName')
      .get(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        UserController.searchUser
      );

    this.router
      .route('/user/settings/public')
      .put(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        SettingsController.updatePublic
      )
      .get(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        UserController.me
      );

    this.router
      .route('/user/settings/profilePicture')
      .post(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        CloudinaryConfig.uploadPicture(),
        SettingsController.newProfilePicture
      );

    this.router
      .route('/user/settings/account')
      .put(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        SettingsController.updateAccount
      )
      .get(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        UserController.me
      );

    this.router
      .route('/user/settings/account/delete')
      .delete(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        VideoController.deleteAllVideos,
        TranslationVideoController.deleteAllTranslationVideos,
        SettingsController.deleteMe
      )
      .get(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        UserController.me
      );

    this.router
      .route('/user/settings/account/email/:token')
      .get(SettingsController.confirmUpdatedEmail);

    this.router
      .route('/user/settings/password')
      .put(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        SettingsController.updatePassword
      )
      .get(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        UserController.me
      );

    this.router
      .route('/user/settings/set/password')
      .put(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        SettingsController.setPassword
      )
      .get(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        UserController.me
      );

    this.router
      .route('/user/documents')
      .get(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        DocumentController.getMyDocuments
      );

    this.router
      .route('/user/video')
      .get(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        VideoController.getMyVideos
      );

    this.router
      .route('/user/translationVideos')
      .get(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        TranslationVideoController.getMyTranslationVideos
      );

    this.router
      .route('/document')
      .post(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        multer.single('file'),
        DocumentController.create
      );

    this.router
      .route('/document/:idDocument')
      .get(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        DocumentController.getById
      )
      .delete(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        DocumentController.delete
      );

    this.router
      .route('/document/:idDocument/like')
      .put(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        DocumentFeaturesController.like
      );

    this.router
      .route('/document/:idDocument/unlike')
      .put(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        DocumentFeaturesController.unlike
      );

    this.router
      .route('/document/:idDocument/favorite')
      .put(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        DocumentFeaturesController.favorite
      );

    this.router
      .route('/document/:idDocument/unfavorite')
      .put(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        DocumentFeaturesController.unfavorite
      );

    this.router
      .route('/document/:idDocument/comment')
      .put(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        DocumentFeaturesController.comment
      );

    this.router
      .route('/document/:idDocument/comment/edit/:idComment')
      .put(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        DocumentFeaturesController.editComment
      );

    this.router
      .route('/document/:idDocument/uncomment/:idComment')
      .delete(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        DocumentFeaturesController.deleteComment
      );

    this.router
      .route('/document/:idDocument/comment/:idComment/like')
      .put(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        DocumentFeaturesController.likeComment
      );

    this.router
      .route('/document/:idDocument/comment/:idComment/unlike')
      .put(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        DocumentFeaturesController.unlikeComment
      );

    this.router
      .route('/document/update/:idDocument')
      .put(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        DocumentController.updateData
      );

    this.router
      .route('/search')
      .get(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        DocumentController.getAllDocuments
      );

    this.router
      .route('/video')
      .post(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        VideoApiAuthentication.uploadVideo(),
        VideoController.create
      );

    this.router.route('/search/video').get(VideoController.searchVideo);

    this.router
      .route('/delete/all/apivideo')
      .delete(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        VideoController.deleteAllVideos
      );

    this.router
      .route('/video/:idApiVideo')
      .get(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        VideoController.getById
      )
      .put(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        VideoController.updateData
      )
      .delete(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        VideoController.delete
      );

    this.router
      .route('/translationVideo')
      .post(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        VideoApiAuthentication.uploadVideo(),
        TranslationVideoController.create
      );

    this.router
      .route('/translationVideo/:idApiVideo')
      .get(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        TranslationVideoController.getById
      )
      .put(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        TranslationVideoController.updateData
      )
      .delete(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        TranslationVideoController.delete
      );

    this.router
      .route('/document/:idDocument/translation')
      .post(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        TranslationController.createNew
      )
      .get(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        TranslationController.getMaster
      );

    this.router
      .route('/translation/:idTranslation')
      .get(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        TranslationController.getById
      )
      .delete(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        TranslationController.delete
      );

    this.router
      .route('/document/:idDocument/all/translation')
      .get(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        TranslationController.getAllByDocumentId
      );

    this.router
      .route('/translation/:idTranslation/fork')
      .post(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        TranslationController.fork
      );

    this.router
      .route('/translation/:idTranslation/zone')
      .post(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        TranslationZoneController.createZone
      );

    this.router
      .route('/translation/:idTranslation/zone/:idZone')
      .get(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        TranslationZoneController.getZone
      )
      .put(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        TranslationZoneController.editZone
      )
      .delete(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        TranslationZoneController.deleteZone
      );

    this.router
      .route('/translation/:idTranslation/pull/master')
      .post(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        TranslationController.pullMaster
      );

    this.router
      .route('/translation/:idTranslation/diff')
      .post(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        TranslationController.diff
      );

    this.router
      .route('/document/:idDocument/editAccess/:idUser/:role')
      .put(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        AccessDocumentController.editAccess
      )
      .delete(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        AccessDocumentController.removeAccess
      );

    this.router
      .route('/video/:idApiVideo/editAccess/:idUser/:role')
      .put(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        AccessDocumentController.editAccess
      )
      .delete(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        AccessDocumentController.removeAccess
      );

    this.router
      .route('/user/follow')
      .put(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        UserFeaturesController.follow
      );

    this.router
      .route('/user/unfollow')
      .put(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        UserFeaturesController.unfollow
      );

    this.router
      .route('/report')
      .post(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        TicketController.create
      )
      .get(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        ApiAuthentication.isAdmin,
        TicketController.getAll
      );

    this.router
      .route('/report/category/:category')
      .get(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        TicketController.getByCategory
      );

    this.router
      .route('/report/status/:status')
      .get(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        TicketController.getByStatus
      );
    this.router
      .route('/report/status/:idTicket/:status')
      .put(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        ApiAuthentication.isAdmin,
        TicketController.changeTicketStatus
      );

    this.router
      .route('/report/:idTicket')
      .get(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        TicketController.getById
      );

    this.router
      .route('/report/user/ticket')
      .get(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        TicketController.getForAuthenticatedUser
      );

    this.router
      .route('/report/user/:userId')
      .get(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        TicketController.getByUser
      );

    this.router.route('/report/user/email/:email').get(
      ApiAuthentication.validJwt,
      ApiAuthentication.retrieveUser,
      TicketController.getByUserEmail
      //TicketController.getByUser
    );

    this.router
      .route('/ticket/response/:ticketId')
      .post(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        TicketController.postNewAnswer
      );

    this.router
      .route('/report/close/:idTicket')
      .put(
        ApiAuthentication.validJwt,
        ApiAuthentication.retrieveUser,
        TicketController.closeTicket
      );

    this.router
      .route('/backoffice/session')
      .post(SessionController.createBackofficeSession);
  }
}

export default Routes;
