import express from 'express';
import Security from 'initializers/express/security';
import bodyParser from 'body-parser';
import Routes from 'initializers/express/routes';
import HandleError from 'initializers/express/handleError';
import NotFound from 'initializers/express/404';
import { basicLogger } from 'initializers/express/winston';

import PassportAuthentication from 'middlewares/passportAuthentication';
import ApiAuthentication from 'middlewares/apiAuthentication';



class Express {
  constructor() {

    this.app = express();
    this.server = require('http').Server(this.app);
    this.io = require('socket.io')(this.server/*, {
      handlePreflightRequest: (req, res) => {
        const headers = {
          "Access-Control-Allow-Headers": "Content-Type, Authorization",
          "Access-Control-Allow-Origin": req.headers.origin, //or the specific origin you want to give access to,
          "Access-Control-Allow-Credentials": true
        };
        res.writeHead(200, headers);
        res.end();
      }
    }*/);

    this.io.set('origins', '*:*');
    // json parser
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use((req, res, next) => {
      req.io = this.io;
      next();
    });
    this.rooms = {};
    this.io.on('connection', (socket) => {

      socket.on('join-room', (data) => {
        if (!data.userId){
          return;
        }
        if (!this.rooms[data.id]) {
          this.rooms[data.id] = {
            id: data.id,
            users: [data.userId]
          }
          socket.join(data.id);
        } else {
          if (!this.rooms[data.id].users.includes(data.userId)) {
            this.rooms[data.id].users.push({ id: data.userId, socket });
            socket.join(data.id);
          } else {
            socket.join(data.id);
          }
        }

        console.log("this.rooms", this.rooms);
      })
      //Emit depuis la route createZone pour la room avec l'id du document
      socket.on('disconnect', function () {
        console.log("Client Disconnect");
        //Nettoyer les rooms de l'user qui vient de se dconnecter
      });
      console.log("ClientConnect");
    });
    /*
      secure http request
      We are not talking about session auth
      Just about http headers actually
    */
    this._setSecurity();

    // winston logger for each request
    this.app.use(basicLogger);

    // Facebook Auth
    PassportAuthentication.googleInitialize();
    PassportAuthentication.facebookInitialize();

    // create routes
    const routes = this._setRoutes();
    this.app.use('/api', routes);

    this.app.use('/medias', [
      ApiAuthentication.validJwt,
      ApiAuthentication.retrieveUser,
      express.static(global.env.upload.folder_path)
    ]);

    this.app.use(NotFound.sendNotFound);

    this.app.use(HandleError.handleError);
  }

  _setSecurity() {
    Security.setSecurity(this.app);
  }

  _setRoutes() {
    this.router = new Routes();
    return this.router.getRouter();
  }

  boot() {
    this.server.listen(global.env.server.port);

    console.log(
      '[v' +
      process.env.npm_package_version +
      '] ' +
      `Dialogram server is listening on ${global.env.server.addr}:${global.env.server.port}`
    );
  }
}

export default Express;
