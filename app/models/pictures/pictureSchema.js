import mongoose from 'mongoose';

const PictureSchema = mongoose.Schema(
  {
    url: {
      type: String,
      default: null
    },
    public_id: {
      type: String,
      default: null
    }
  },
  {
    strict: true,
    _id: false
  }
);

export default PictureSchema;
