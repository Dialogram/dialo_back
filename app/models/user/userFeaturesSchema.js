import mongoose from 'mongoose';

const UserFeaturesSchema = mongoose.Schema(
  {
    follows: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
      }
    ],
    followers: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
      }
    ],
    documentsLiked: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Documents'
      }
    ],
    documentsCommented: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Documents'
      }
    ],
    documentsFavorite: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Documents'
      }
    ]
  },
  {
    strict: true
  }
);

export default UserFeaturesSchema;
