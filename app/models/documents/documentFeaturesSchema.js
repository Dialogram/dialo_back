import mongoose, { Schema } from 'mongoose';

const DocumentFeaturesSchema = mongoose.Schema(
  {
    likes: [
      {
        type: Schema.Types.ObjectId,
        ref: 'User'
      }
    ],
    favorites: [
      {
        type: Schema.Types.ObjectId,
        ref: 'User'
      }
    ],
    comments: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Comment'
      }
    ]
  },
  {
    strict: true
  }
);

export default DocumentFeaturesSchema;
