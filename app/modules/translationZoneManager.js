import TranslationZone from 'models/translationZones/translationZoneSchema';

class TranslationZoneManager {
  static async findById(idZone) {
    return await TranslationZone.findById(idZone);
  }

  static async findByIdAndPopulateVideos(idZone) {
    return await TranslationZone.findById(idZone).populate({
      path: 'idVideo',
      model: 'TranslationVideo',
      select: {
        _id: 1,
        title: 1,
        idOwner: 1,
        'assets.hls': 1
      }
    });
  }

  static async findByIdDocument(idDocument) {
    return await TranslationZone.find({ idDocument: idDocument });
  }

  static async findZoneById(translation, idZone) {
    let zone;

    if (
      !(zone = await TranslationZone.findOne({
        _id: translation._id,
        zones: [`${idZone}`]
      }))
    ) {
      return;
    }
    return zone;
  }
}

export default TranslationZoneManager;
