import VideoSerializer from 'serializers/videos/videoSerializer';
import VideoCreateContext from 'contexts/videos/videoCreateContext';
import VideoSaveContext from 'contexts/videos/videoSaveContext';
import VideoDeleteOneContext from 'contexts/videos/videoDeleteOneContext';
import PaginationHelper from 'helpers/pagination/paginationHelper';
import VideoManager from 'modules/videoManager';
import PaginationSerializer from 'serializers/paginations/paginationSerializer';
import Singleton from 'controllers/singleton/singleton';
import AccessRightManager from 'modules/accessRightManager';
import moment from 'moment';
import {
  ApiNotFoundError,
  ApiForbiddenError,
  ApiUnauthorizedError,
  ApiServerError,
  ApiBadRequestError
} from 'modules/apiError';

let client = new Singleton().getApiVideo();

class VideoController {
  static async create(req, res, next) {
    let createdVideo;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (
      !req.body.title ||
      !req.body.public ||
      !req.body.category ||
      !req.file
    ) {
      return next(
        new ApiBadRequestError('Tous les champs doivent être renseignés')
      );
    }
    const result = client.videos.upload(req.file.path, {
      title: req.body.title,
      description: req.body.description
    });
    result
      .then(async video => {
        try {
          createdVideo = await VideoCreateContext.call(req, video);
        } catch (err) {
          return next(err);
        }

        try {
          createdVideo.access['role'] = await AccessRightManager.GetRole(
            createdVideo,
            req.user._id
          );
        } catch (err) {
          return next(new ApiBadRequestError(err));
        }

        return res.json({
          data: [VideoSerializer(createdVideo)],
          includes: []
        });
      })
      .catch(err => {
        return next(new ApiServerError(err.message));
      });
    result
      .then(async video => {
        try {
          createdVideo = await VideoCreateContext.call(req, video);
        } catch (err) {
          return next(err);
        }
        return res.json({
          data: VideoSerializer(createdVideo)
        });
      })
      .catch(err => {
        return next(new ApiServerError(err.message));
      });
  }

  static searchVideo(req, res, next) {
    let result = client.videos.search({ currentPage: 1, pageSize: 25 });

    result.then(function(videos) {
      let video;
      for (let x = 0; x < videos.length; x += 1) {
        if (Object.prototype.hasOwnProperty.call(videos, x)) {
          video = videos[x];
        }
      }
    });
    return res.json({
      data: {}
    });
  }

  static async getById(req, res, next) {
    let video;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (!(video = await VideoManager.findByIdApiVideo(req.params.idApiVideo))) {
      return next(new ApiNotFoundError('Ressource introuvable'));
    }

    try {
      await AccessRightManager.PopulateAccess(video, req.user._id);
    } catch (err) {
      return next(
        new ApiBadRequestError(
          'Erreur pendant la récupération des accès utilisateur' + err
        )
      );
    }

    if (video.public == false && video.access.role == 'NONE') {
      return next(new ApiForbiddenError('Accès non autorisé'));
    }

    return res.json({
      data: VideoSerializer(video)
    });
  }

  static async getMyVideos(req, res, next) {
    let video;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }

    const population = {
      path: 'features.comments',
      populate: {
        path: 'ownerId',
        select: {
          _id: 1,
          nickName: 1,
          'profile.firstName': 1,
          'profile.lastName': 1,
          'profile.certificated': 1,
          'profile.profilePicture': 1
        }
      }
    };

    const pagination = await PaginationHelper.setDefaultPagination(req.query);
    let options = await PaginationHelper.parseOption(pagination, req);
    options.populate = population;
    const filter = await PaginationHelper.cleanFilter(req.query);
    filter.idOwner = req.user._id;

    try {
      video = await VideoManager.pagination(filter, options);
    } catch (err) {
      return next(err);
    }

    for (let i = 0; video.docs[i]; i++) {
      try {
        await AccessRightManager.PopulateAccess(video.docs[i], req.user._id);
      } catch (err) {
        return next(
          new ApiBadRequestError(
            'Erreur pendant la récupération des accès utilisateur'
          )
        );
      }
    }

    return res.json({
      data: video.docs.map(item => VideoSerializer(item)),
      includes: [PaginationSerializer(video)]
    });
  }

  static async updateData(req, res, next) {
    let video;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (!(video = await VideoManager.findByIdApiVideo(req.params.idApiVideo))) {
      return next(new ApiNotFoundError('Ressource introuvable'));
    }

    try {
      await AccessRightManager.PopulateAccess(video, req.user._id);
    } catch (err) {
      return next(
        new ApiBadRequestError(
          'Erreur pendant la récupération des accès utilisateur'
        )
      );
    }

    if (
      video.public != true &&
      (document.access.role == 'NONE' || document.access.role == 'collaborator')
    ) {
      return next(new ApiForbiddenError('Permissions insuffisantes'));
    }

    if (
      !req.body.title ||
      !req.body.description ||
      !req.body.public ||
      !req.body.category
    ) {
      return next(
        new ApiBadRequestError('Tous les champs doivent être renseignés')
      );
    }
    video.title = req.body.title;
    video.description = req.body.description;
    video.timestamp = moment();
    client.videos.update(req.params.idApiVideo, {
      title: req.body.title,
      description: req.body.description
    });
    video.public = req.body.public;
    video.category = req.body.category;
    try {
      await VideoSaveContext.call(video);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: VideoSerializer(video)
    });
  }

  static async delete(req, res, next) {
    let video;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (!(video = await VideoManager.findByIdApiVideo(req.params.idApiVideo))) {
      return next(new ApiNotFoundError('Ressource introuvable'));
    }
    if (
      !(await VideoManager.findByVideoAndOwner(
        req.user._id,
        req.params.idApiVideo
      ))
    ) {
      return next(new ApiForbiddenError('Cette video ne vous appartient pas'));
    }

    try {
      await AccessRightManager.PopulateAccess(video, req.user._id);
    } catch (err) {
      return next(
        new ApiBadRequestError(
          'Erreur pendant la récupération des accès utilisateur'
        )
      );
    }

    if (video.access.role != 'owner') {
      return next(new ApiForbiddenError('Accès non autorisé'));
    }

    let result = client.videos.delete(req.params.idApiVideo);
    result
      .then(async statusCode => {
        try {
          await VideoDeleteOneContext.call(video, { _id: video.id });
        } catch (err) {
          return next(err);
        }
        return res.json({
          data: {}
        });
      })
      .catch(err => {
        return next(new ApiServerError(err.message));
      });
  }

  static async deleteAllVideos(req, res, next) {
    let videos;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (!(videos = await VideoManager.findByOwner(req.user._id))) {
      next();
    }

    for (let i = 0; videos[i]; i++) {
      try {
        await AccessRightManager.PopulateAccess(videos[i], req.user._id);
      } catch (err) {
        return next(
          new ApiBadRequestError(
            'Erreur pendant la récupération des accès utilisateur'
          )
        );
      }
    }

    for (let i = 0; i < videos.length; i += 1) {
      if (videos[i].access.role != 'owner') {
        return next(new ApiForbiddenError('Accès non autorisé'));
      }

      try {
        await client.videos.delete(videos[i].idApiVideo);
        await VideoDeleteOneContext.call(videos[i], { _id: videos[i].id });
      } catch (err) {
        return next(err);
      }
    }
    next();
  }
}

export default VideoController;
