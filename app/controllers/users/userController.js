import UserCreateUserContext from 'contexts/users/userCreateUserContext';
import UserCreateSessionContext from 'contexts/sessions/userCreateSessionContext';
import UserTokenContext from 'contexts/users/userTokenContext';
import SessionSerializer from 'serializers/users/sessionSerializer';
import UserSerializer from 'serializers/users/userSerializer';
import UserManager from 'modules/userManager';
import UserHelper from 'helpers/users/userHelper';
import NoReplyMailHelper from 'helpers/users/noreplyMailHelper';

import validator from 'email-validator';
import jwt from 'jsonwebtoken';

import {
  ApiUnauthorizedError,
  ApiNotFoundError,
  ApiForbiddenError,
  ApiBadRequestError
} from 'modules/apiError';

class UserController {
  static async refreshToken(req, res, next) {
    let user;

    try {
      user = await UserManager.findByRefreshToken(req.params.token);
      for (let session of user.sessions) {
        if (session.refreshToken === req.params.token) {
          session.token = jwt.sign(
            { userId: user._id },
            global.env.key.jwtSecret,
            {
              algorithm: 'HS256',
              expiresIn: global.env.key.accessTokenExpired
            }
          );
          session.expireAt =
            Date.now() + global.env.key.accessTokenExpired * 1000;
          return res.json({
            data: SessionSerializer({ user, session })
          });
        }
      }
    } catch (err) {
      return next(new ApiUnauthorizedError(err.message));
    }
    return res.json({
      data: SessionSerializer({ user })
    });
  }

  static async create(req, res, next) {
    let user;
    let givenParams;

    if (
      !req.body.email ||
      !req.body.nickName ||
      !req.body.profile.firstName ||
      !req.body.profile.lastName ||
      !req.body.password
    ) {
      return next(
        new ApiBadRequestError(
          'Veuillez renseigner tous les champs afin de créer un compte'
        )
      );
    }
    if (validator.validate(req.body.email) == false) {
      return next(new ApiBadRequestError('Adresse courriel invalide'));
    }
    if ((user = await UserManager.findByEmail(req.body.email)) != null) {
      return next(new ApiForbiddenError('Adresse courriel déjà utilisée'));
    }
    if ((user = await UserManager.findByNickName(req.body.nickName)) != null) {
      return next(new ApiForbiddenError('Ce pseudonyme est déjà utilisé'));
    }
    if (req.body.password.length < 8 || req.body.password.length > 22) {
      return next(
        new ApiForbiddenError(
          'La taille du mot de passe doit être comprise entre 8 et 22 caractères'
        )
      );
    }
    givenParams = {
      nickName: req.body.nickName,
      profile: {
        createdWith: {
          provider: 'Dialogram',
          userId: null
        },
        firstName: req.body.profile.firstName,
        lastName: req.body.profile.lastName,
        profilePicture: {
          url: UserHelper.getAvatarURL(
            req.body.profile.firstName,
            req.body.profile.lastName
          )
        }
      },
      email: req.body.email,
      password: req.body.password
    };
    try {
      user = await UserCreateUserContext.call(givenParams);
      await UserCreateSessionContext.createSession(
        user,
        UserHelper.getDeviceName(req),
        global.env.key.accessTokenExpired,
        'Dialogram'
      );
      user = await UserTokenContext.createConfirmAccountToken(
        user,
        global.env.key.accessTokenExpired
      );
      NoReplyMailHelper.mailConfirmAccount(user);
    } catch (err) {
      return next(new ApiBadRequestError(err.message));
    }
    return res.json({
      data: UserSerializer(user),
      includes: [SessionSerializer({ user })]
    });
  }

  static async createFromFacebook(req, res, next) {
    let user;
    let cloudinaryUrl;

    if (!req.user) {
      return next(new ApiNotFoundError('Utilisateur introuvable'));
    }
    if (!(user = await UserManager.findByProvider(req.user.id))) {
      try {
        if (
          !(cloudinaryUrl = await UserHelper.uploadPictureFromURL(
            req.user.profile.profilePicture.url
          ))
        ) {
          cloudinaryUrl = UserHelper.getAvatarURL(
            req.user.profile.firstName,
            req.user.profile.lastName
          );
        } else {
          req.user.profile.profilePicture.url =
            cloudinaryUrl.eager[0].secure_url;
          req.user.profile.profilePicture.public_id = cloudinaryUrl.public_id;
        }
      } catch (err) {
        return next(err);
      }
      try {
        user = await UserCreateUserContext.call(req.user);
        await UserCreateSessionContext.createSession(
          user,
          UserHelper.getDeviceName(req),
          global.env.key.accessTokenExpired,
          'Facebook'
        );
        user = await UserTokenContext.createConfirmAccountToken(
          user,
          global.env.key.accessTokenExpired
        );
        NoReplyMailHelper.mailConfirmAccount(user);
        NoReplyMailHelper.mailCreateUser(user);
      } catch (err) {
        return next(new ApiBadRequestError(err.message));
      }
    } else {
      try {
        await UserCreateSessionContext.createSession(
          user,
          UserHelper.getDeviceName(req),
          global.env.key.accessTokenExpired,
          'Facebook'
        );
      } catch (err) {
        return next(err);
      }
    }
    return res.json({
      data: UserSerializer(user),
      includes: [SessionSerializer({ user })]
    });
  }

  static async createFromGoogle(req, res, next) {
    let user;
    let cloudinaryUrl;

    if (!req.user) {
      return next(new ApiNotFoundError('Utilisateur introuvable'));
    }
    if (!(user = await UserManager.findByProvider(req.user.id))) {
      try {
        if (
          !(cloudinaryUrl = await UserHelper.uploadPictureFromURL(
            req.user.profile.profilePicture.url
          ))
        ) {
          cloudinaryUrl = UserHelper.getAvatarURL(
            req.user.profile.firstName,
            req.user.profile.lastName
          );
        } else {
          req.user.profile.profilePicture.url =
            cloudinaryUrl.eager[0].secure_url;
          req.user.profile.profilePicture.public_id = cloudinaryUrl.public_id;
        }
      } catch (err) {
        return next(err);
      }
      try {
        user = await UserCreateUserContext.call(req.user);
        await UserCreateSessionContext.createSession(
          user,
          UserHelper.getDeviceName(req),
          global.env.key.accessTokenExpired,
          'Google'
        );
        user = await UserTokenContext.createConfirmAccountToken(
          user,
          global.env.key.accessTokenExpired
        );
        NoReplyMailHelper.mailConfirmAccount(user);
        NoReplyMailHelper.mailCreateUser(user);
      } catch (err) {
        return next(new ApiBadRequestError(err.message));
      }
    } else {
      try {
        await UserCreateSessionContext.createSession(
          user,
          UserHelper.getDeviceName(req),
          global.env.key.accessTokenExpired,
          'Google'
        );
      } catch (err) {
        return next(err);
      }
    }
    return res.json({
      data: UserSerializer(user),
      includes: [SessionSerializer({ user })]
    });
  }

  static async me(req, res) {
    return res.json({
      data: UserSerializer(req.user)
    });
  }

  static async getUser(req, res, next) {
    let user;

    if (!(user = await UserManager.findById(req.params.idUser))) {
      return next(new ApiNotFoundError('Utilisateur introuvable'));
    }
    return res.json({
      data: UserSerializer(user)
    });
  }

  static async searchUser(req, res) {
    let user;

    user = await UserManager.searchByNickName(req.params.nickName);
    return res.json({
      data: user.map(item => UserSerializer(item)),
      includes: []
    });
  }
}

export default UserController;
