import mongoose from 'mongoose';
import Schema from 'mongoose';

const AccessRightSchema = mongoose.Schema({
  idOwner: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  moderator: {
    type: [String],
    ref: 'User'
  },
  collaborator: {
    type: [String],
    ref: 'User'
  }
});

export default AccessRightSchema;
