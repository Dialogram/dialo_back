const translationVideoSerializer = translationVideo => {
  if (!translationVideo) {
    return {};
  }

  const data = {
    type: 'translation_videos',
    id: translationVideo.id,
    idOwner: translationVideo.idOwner,
    idApiVideo: translationVideo.idApiVideo,
    title: translationVideo.title,
    description: translationVideo.description,
    public: translationVideo.public,
    publishedAt: translationVideo.publishedAt,
    tags: translationVideo.tags,
    metadata: translationVideo.metadata,
    source: {
      type: translationVideo.source.type,
      uri: translationVideo.source.uri
    },
    assets: {
      iframe: translationVideo.assets.iframe,
      player: translationVideo.assets.player,
      hls: translationVideo.assets.hls,
      thumbnail: translationVideo.assets.thumbnail
    },
    timestamp: translationVideo.timestamp
  };
  return data;
};

export default translationVideoSerializer;
