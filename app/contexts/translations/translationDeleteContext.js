import { ApiServerError } from 'modules/apiError';

class TranslationDeleteContext {
  static async call(translation, query) {
    await translation.remove(query, err => {
      if (err) {
        throw new ApiServerError(err.message);
      }
    });
  }
}

export default TranslationDeleteContext;
