import TranslationZoneSerializer from 'serializers/translationZones/translationZoneSerializer';
import TranslationZoneCreateContext from 'contexts/translationZones/translationZoneCreateContext';
import TranslationZoneDuplicateContext from 'contexts/translationZones/translationZoneDuplicateContext';
import TranslationZoneSaveContext from 'contexts/translationZones/translationZoneSaveContext';
import TranslationZoneDeleteContext from 'contexts/translationZones/translationZoneDeleteContext';
import TranslationSaveContext from 'contexts/translations/translationSaveContext';
import TranslationZoneManager from 'modules/translationZoneManager';
import TranslationManager from 'modules/translationManager';

import {
  ApiNotFoundError,
  ApiBadRequestError,
  ApiForbiddenError
} from 'modules/apiError';

class TranslationZoneController {
  static async createZone(req, res, next) {
    let translation;
    let zone;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (
      !(translation = await TranslationManager.findById(
        req.params.idTranslation
      ))
    ) {
      return next(new ApiNotFoundError('Traduction introuvable'));
    }
    if (
      !req.body.x ||
      !req.body.y ||
      !req.body.width ||
      !req.body.height ||
      !req.body.page
    ) {
      return next(
        new ApiBadRequestError(
          'Les coordonnées de la traduction doivent être renseignées'
        )
      );
    }
    if (translation.isMaster === true) {
      req.body.state = 'MASTER';
    } else {
      req.body.state = 'NEW';
    }
    try {
      zone = await TranslationZoneCreateContext.call(req.body);
      translation.zones.push(zone);
      TranslationSaveContext.call(translation);
    } catch (err) {
      return next(err);
    }
    req.io.to(translation.idDocument).emit('new_zone', { data: zone, emitBy: req.user._id });
    return res.json({
      data: TranslationZoneSerializer(zone)
    });
  }

  static async getZone(req, res, next) {
    let zone;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (!(await TranslationManager.findById(req.params.idTranslation))) {
      return next(new ApiNotFoundError('Traduction introuvable'));
    }
    if (
      !(zone = await TranslationZoneManager.findByIdAndPopulateVideos(
        req.params.idZone
      ))
    ) {
      return next(new ApiNotFoundError('Zone introuvable'));
    }
    return res.json({
      data: TranslationZoneSerializer(zone)
    });
  }

  static async editZone(req, res, next) {
    let zone;
    let translation;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (
      !(translation = await TranslationManager.findById(
        req.params.idTranslation
      ))
    ) {
      return next(new ApiNotFoundError('Traduction introuvable'));
    }
    if (!(zone = await TranslationZoneManager.findById(req.params.idZone))) {
      return next(new ApiNotFoundError('Zone introuvable'));
    }
    if (
      !req.body.x ||
      !req.body.y ||
      !req.body.width ||
      !req.body.height ||
      !req.body.page
    ) {
      return next(
        new ApiBadRequestError(
          'Les coordonnées de la traduction doivent être renseignées'
        )
      );
    }
    zone.x = req.body.x;
    zone.y = req.body.y;
    zone.width = req.body.width;
    zone.height = req.body.height;
    zone.idVideo = req.body.idVideo;
    zone.page = req.body.page;
    if (translation.isMaster == true) {
      zone.state = 'MASTER';
    } else if (zone.state == 'MASTER' && translation.isMaster == false) {
      zone.state = 'MASTER_UPDATE';
    }
    try {
      zone = await TranslationZoneSaveContext.call(zone);
    } catch (err) {
      return next(err);
    }
    req.io.to(translation.idDocument).emit('new_zone', { data: zone, emitBy: req.user._id });
    return res.json({
      data: TranslationZoneSerializer(zone)
    });
  }

  static async deleteZone(req, res, next) {
    let translation;
    let zone;

    if (!req.user) {
      return next(new ApiForbiddenError('Utilisateur introuvable'));
    }
    if (
      !(translation = await TranslationManager.findById(
        req.params.idTranslation
      ))
    ) {
      return next(new ApiNotFoundError('Traduction introuvable'));
    }
    if (!(zone = await TranslationZoneManager.findById(req.params.idZone))) {
      return next(new ApiNotFoundError('Zone introuvable'));
    }
    try {
      if (translation.isMaster == true || zone.state == 'NEW') {
        for (let i = 0; i < translation.zones.length; i++) {
          if (translation.zones[i] == req.params.idZone) {
            translation.zones.splice(i, 1);
            break;
          }
        }
        await TranslationZoneDeleteContext.call(zone);
        await TranslationSaveContext.call(translation);
        return res.json({
          data: {}
        });
      }
      zone.state = 'DELETED';
      zone = await TranslationZoneSaveContext.call(zone);
    } catch (err) {
      return next(err);
    }
    req.io.to(translation.idDocument).emit('new_zone', { data: zone, emitBy: req.user._id });
    return res.json({
      data: TranslationZoneSerializer(zone)
    });
  }

  static async forkZones(zones) {
    let forkedZone;
    let forkedZones = [];

    for (let i = 0; i < zones.length; i++) {
      if (!(forkedZone = await TranslationZoneManager.findById(zones[i]))) {
        throw new ApiNotFoundError('Zone introuvable');
      }
      try {
        forkedZone = await TranslationZoneDuplicateContext.call(forkedZone);
        forkedZones.push(forkedZone);
      } catch (err) {
        throw new ApiBadRequestError(err.message);
      }
    }
    return forkedZones;
  }
}

export default TranslationZoneController;
