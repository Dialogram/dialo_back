import mongoose from 'mongoose';
import Schema from 'mongoose';
import moment from 'moment';
import mongoosePaginate from 'mongoose-paginate-v2';

const TranslationVideoSchema = mongoose.Schema(
  {
    idOwner: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    idApiVideo: {
      type: String,
      required: true
    },
    title: {
      type: String,
      required: true
    },
    description: {
      type: String,
      default: null
    },
    public: {
      type: Boolean,
      default: false
    },
    publishedAt: {
      type: String,
      default: null
    },
    tags: {
      type: [String],
      default: []
    },
    metadata: {
      type: [String],
      default: []
    },
    source: {
      type: {
        type: String,
        required: true
      },
      uri: {
        type: String,
        required: true
      }
    },
    assets: {
      iframe: {
        type: String,
        required: true
      },
      player: {
        type: String,
        required: true
      },
      hls: {
        type: String,
        required: true
      },
      thumbnail: {
        type: String,
        required: true
      }
    },
    timestamp: {
      type: Number,
      default: moment().unix(1318874398806)
    }
  },
  {
    strict: true
  }
);

TranslationVideoSchema.plugin(mongoosePaginate);
const TranslationVideo = mongoose.model(
  'TranslationVideo',
  TranslationVideoSchema
);

export default TranslationVideo;
