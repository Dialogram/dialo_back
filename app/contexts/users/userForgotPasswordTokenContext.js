import jwt from 'jsonwebtoken';

class UserForgotPasswordTokenContext {
  static async createForgotPasswordToken(user, expirationTime) {
    const token = jwt.sign({ userId: user._id }, global.env.key.jwtSecret, {
      algorithm: 'HS256',
      expiresIn: expirationTime
    });

    user.tokenPasswordReset = token;
    try {
      await user.save();
    } catch (err) {
      throw new ApiServerError(err.message);
    }
    return user;
  }
}

export default UserForgotPasswordTokenContext;
