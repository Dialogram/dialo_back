const translationZoneSerializer = translationZone => {
  if (!translationZone) {
    return {};
  }

  const data = {
    id: translationZone.id,
    x: translationZone.x,
    y: translationZone.y,
    width: translationZone.width,
    height: translationZone.height,
    idVideo: translationZone.idVideo,
    page: translationZone.page,
    idMaster: translationZone.idMaster,
    state: translationZone.state
  };
  return data;
};

export default translationZoneSerializer;
