import jwt from 'jsonwebtoken';
import UserHelper from 'helpers/users/userHelper';
import UserManager from 'modules/userManager';
import bcrypt from 'bcrypt-nodejs';

import {
  ApiBadRequestError,
  ApiUnauthorizedError,
  ApiNotFoundError
} from 'modules/apiError';

class UserCreateSessionContext {
  static async callBackOffice(req) {
    let givenParams = req.body;
    let user;

    if (!givenParams.email || !givenParams.password) {
      throw new ApiBadRequestError(
        "L'adresse courriel ainsi que le mot de passe doivent être renseignés"
      );
    }
    try {
      user = await UserManager.findByEmail(req.body.email);
    } catch (err) {
      throw new ApiUnauthorizedError(
        'Pseudonyme et/ou mot de passe invalide(s)'
      );
    }
    if (
      user == null ||
      !bcrypt.compareSync(givenParams.password, user.password)
    ) {
      throw new ApiUnauthorizedError(
        'Pseudonyme et/ou mot de passe invalide(s)'
      );
    }
    if (user.role !== 'admin') {
      throw new ApiUnauthorizedError(
        "Vous n'avez pas l'authorisation d'accéder à cette ressource."
      );
    }
    return this.createSession(
      user,
      UserHelper.getDeviceName(req),
      global.env.key.accessTokenExpired,
      'Dialogram'
    );
  }

  static async call(req) {
    let givenParams = req.body;
    let user;

    if (!givenParams.email || !givenParams.password) {
      throw new ApiBadRequestError(
        "L'adresse courriel ainsi que le mot de passe doivent être renseignés"
      );
    }
    try {
      user = await UserManager.findByEmail(req.body.email);
    } catch (err) {
      throw new ApiUnauthorizedError(
        'Pseudonyme et/ou mot de passe invalide(s)'
      );
    }
    if (
      user == null ||
      !bcrypt.compareSync(givenParams.password, user.password)
    ) {
      throw new ApiUnauthorizedError(
        'Pseudonyme et/ou mot de passe invalide(s)'
      );
    }
    return this.createSession(
      user,
      UserHelper.getDeviceName(req),
      global.env.key.accessTokenExpired,
      'Dialogram'
    );
  }

  static async createSession(user, device, expirationTime, provider) {
    const token = jwt.sign({ userId: user._id }, global.env.key.jwtSecret, {
      algorithm: 'HS256',
      expiresIn: global.env.key.accessTokenExpired
    });
    const refreshToken = jwt.sign(
      { userId: user._id, accessToken: token },
      global.env.key.jwtSecret,
      {
        algorithm: 'HS256',
        expiresIn: global.env.key.refreshTokenExpired
      }
    );
    let session = {
      token,
      refreshToken: refreshToken,
      expireAt: Date.now() + expirationTime,
      provider: provider,
      device: {
        userAgent: null,
        OS: null
      }
    };
    if (device) {
      session.device.userAgent = device.userAgent;
      session.device.OS = device.OS;
    }
    if (user.sessions instanceof Array) {
      user.sessions.push(session);
    } else {
      user.sessions = [session];
    }

    try {
      await user.save();
    } catch (err) {
      throw new ApiBadRequestError(err.message);
    }
    return user;
  }
}

export default UserCreateSessionContext;
