import { ApiBadRequestError } from 'modules/apiError';

class CommentSaveContext {
  static async call(comment) {
    try {
      await comment.save();
    } catch (err) {
      throw new ApiBadRequestError(err.message);
    }
    return comment;
  }
}

export default CommentSaveContext;
